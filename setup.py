# -*- coding: utf-8 -*-

from pathlib import Path

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup
# from distutils.core import setup

import versioneer


def get_packages():
    try:
        return find_packages('.')
    except NameError:
        packages = ['miscmics']
        for init in Path(*packages).glob('*/__init__.py'):
            subpack = str(init.parent).replace('/', '.')
            if subpack not in packages:
                packages.append(subpack)
        return packages

def get_datafiles():
    # _config_path = None
    # mod = _import_file('_config', _config_path)
    # dir_name = mod.__marker_db_dir
    # return [(dir_name, ['./res/data/abids.csv', './res/data/abids.json'])]
    return []

def get_scripts():
    # script_dir = Path('./res/scripts/')
    # return [str(p) for p in script_dir.glob('*.bat')]
    return []

# def get_pkgfiles():
#     """Files stored in the package
#     """
#     # ret = {
#     #     'orangecontrib.cellinspector.widgets': ['icons/*'],
#     # }
#     return {}

def get_requires():
    """for full functionality needed
    """
    base = [
        'pathlib',
        'numpy',
        # 'pillow', dependency of imageio
        'imageio',
        'dataclasses',
        'opencv-python',
        'scipy',
        'scikit-learn',
        'h5py'

    ]
    # full = base + get_extra_requires()
    # ret = {'base': base, 'full': full}
    return base

# def get_extra_requires():
#     """some extras, mostly dev related
#     """
#     extras = {
#         'dev': ['versioneer', 'wheel', 'CProfileV', 'pytest', 'pytest-qt'],
#         'img': ['libtiff', 'Orange3-ImageAnalytics'],
#     }
# 
#     return extras

def get_keywords():
    """get/parse the keywords
    """
    kwords = (
        'microscopy',
        'high-content',
        'multiplex',
        'image',
        'metadata',
    )

    return kwords

def main():
    """Run the installation
    """

    setup(

        name='miscmics',
        version=versioneer.get_version(),
        cmdclass=versioneer.get_cmdclass(),

        # packages=find_packages('src'),
        packages=get_packages(),

        description='Handle multiplexed images',
        long_description='Tools and utils + marker infromation to access' +\
                         'MICS data in a uniform, consisten fashion',

        # package_data=get_pkgfiles(),
        # include_package_data=True,

        python_requires='>=3',

        install_requires=get_requires(),
        # extras_require=get_extra_requires(),

        keywords=get_keywords(),
        # namespace_packages=['orangecontrib'],
        # entry_points=get_entry(),
        # scripts=get_scripts(),
        # packages=setuptools.find_packages(),
        # package_data={'segtool': ['./res/data/*.csv', './res/data/*.json']},
        # data_files=get_datafiles(),
        # license='Creative Commons Attribution-Noncommercial-Share Alike license',
        # long_description='Analyse Cells in Orange',
    )


if __name__ == '__main__':
    main()
