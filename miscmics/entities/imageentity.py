"""Basic Entity in an image stack, that claims some pixel in a multiplexed
image stack"""

import warnings
from typing import Tuple, List
from dataclasses import field, dataclass

import numpy as np

# TODO allow scikti-image backend
from cv2 import drawContours

from .entity import Entity, EntityType


@dataclass
class ImageEntity(Entity):
    """Entity for use with miscmics.ImageStack

    Data class members should never be considered immutable.
    All the setting happens in ImageEntity.update_contour
    """
    mask: np.ndarray = field(default_factory=list)

    # List[RowSlice, ColSlice]
    slc: Tuple[slice] = field(default_factory=tuple)
    contour: List[np.ndarray] = field(default_factory=list)
    # contour center point
    center: np.ndarray = field(default_factory=list)

    # Contour[ Shape[ Point[col, row] ]]
    int_contour: List[np.ndarray] = field(default_factory=list)

    # separate int BBox and float BBox
    # BoundingBox[ Point[col, row] ]
    bbox: np.ndarray = field(default_factory=list) # is int

    def __post_init__(self):
        if self.contour:
            self.update_contour(self.contour)

    # Seemingly dataclass messses with inheritance of __hash__
    def __hash__(self): # pylint: disable=useless-super-delegation
        return super().__hash__()

    def _set_int_contour(self):
        self.int_contour = []
        for cnt in self.contour:
            cnt = np.array(cnt)
            # center = np.sum(cnt, 0) / len(cnt)
            # dirs = cnt - center
            # int_contour.append(np.round(center) + np.round(dirs))
            self.int_contour.append(np.round(cnt).astype(int))

    def _set_bbox_and_slice(self):
        # split apln points in inner of contours
        mins = []
        maxs = []
        # Boundingbox is defined by two corner points
        self.bbox = np.empty((2, 2), dtype=int)
        for int_cnt in self.int_contour:
            point = int_cnt.reshape(-1, 2)
            mins.append(np.min(point, 0))
            maxs.append(np.max(point, 0))

        self.bbox[0] = np.min(mins, 0)
        self.bbox[1] = np.max(maxs, 0)

        bounds = (self.bbox.T + [0, 1]).T
        self.slc = (
            slice(*bounds[:, 1].astype(int)),
            slice(*bounds[:, 0].astype(int)),
        )

    def _set_mask(self):
        left_top, right_bottom = self.bbox

        height, width = (right_bottom - left_top)

        self.mask = np.zeros((width+1, height+1), np.uint8)

        drawContours(
            image=self.mask,
            contours=self.int_contour,
            contourIdx=-1,
            color=1,
            thickness=-1,
            # lineType=None,
            # hierarchy=None,
            # maxLevel=None,
            offset=tuple(-left_top),
        )

        self.mask = self.mask.astype(bool)

    def _set_contour_center(self):
        """ Geometrical center/mass center with euqal weighted points
        """
        self.center = np.zeros(2, float)

        point_count = 0
        for shape in self.contour:
            self.center += np.sum(shape, axis=0)
            point_count += len(shape)

        self.center /= point_count

    def update_contour(self, new_contour: List[List[List[float]]]):
        """Updates the entity contour and all derived attributes

        Expecting an iterables with the dimensions  (S, Ns, 2),
        where S is the number of shapes (closed polygons) in the contour
        Ns is the number of 2d points making up the shape s < S

        Notes
        -----
        Will set the `slc`, `mask`, `int_contour`, `center`.
        """

        if self.etype == EntityType.Historic:
            warnings.warn(f'Can not set contour for historic entity {self.eid}')
            self.contour = []
            return

        try:
            # might raise index errors
            self.contour = [np.array(cnt) for cnt in new_contour]
            self._set_int_contour()

            # might raise value errors (zero size arrays)
            self._set_bbox_and_slice()

        except (IndexError, ValueError):
            raise ValueError(f'Invalid contour can not be set: {new_contour} ')

        self._set_mask()

        self._set_contour_center()

