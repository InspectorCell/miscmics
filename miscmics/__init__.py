# -*- coding: utf-8 -*-
"""Combined classes and functions to handle, read, store and exchange
multiplexed immunostaining images, their annotations and metadata
"""

from ._version import get_versions
__version__ = get_versions()['version']
del get_versions
