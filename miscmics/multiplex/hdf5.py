"""Writing and reading ImageStack and their meta data into HDF5 files
"""
from typing import Union, Dict
from pathlib import Path

import h5py
import numpy as np

from .multiplex import ImageStack


# TODO contextlib? must do interface anyways...
class HDF5Stacks:
    """HDF5 file handle class

    Provides context, file() like access to ImagesStacks
    stored in HDF5 files. Defines which and how metadata in
    imagestacks is stored
    """

    def __init__(self, hdf5file: Union[Path, str], mode: str = 'r'):
        self.root = h5py.File(hdf5file, mode)

    def add_imagestack(self, stack: ImageStack):
        """Generates an ndimage from imagestack and stores
        it in hdf5 file"""

        stack_meta = stack.meta.copy()

        try:
            stack_name = stack_meta.pop('name')
        except KeyError:
            stack_name = f'ImageStack {len(self.root.keys())}'
        try:
            stack_uri = stack_meta.pop('uri')
        except KeyError as err:
            raise ValueError('ImageStack must have uri meta data') from err

        ims_group = self.root.create_group(stack_name, track_order=True)
        ims_group.attrs['uri'] = stack_uri
        for key, value in stack_meta.items():
            ims_group.attrs[key] = value

        ims_group.attrs['names'] = [ch.meta['name'] for ch in stack.data]

        for chan in stack.data:
            chan_meta = chan.meta.copy()
            chan_dat = ims_group.create_dataset(
                name=chan_meta.pop('name'), data=chan)
            for key, val in chan_meta.items():
                chan_dat.attrs[key] = val
            set_attr_gcimage(chan_dat)

    def read(self, field: Dict = None):
        """
        Reads image stacks from opend HDF5Stacks file

        Parameters
        ----------
        Metadata : Dict
            Dictionary
        """

        if field is not None:
            # TODO implement me!
            raise NotImplementedError()

        ret = []
        for ims_name, group in self.root.items():
            # create stack with metadata
            ims_meta = {'name': ims_name}
            ims_meta.update(group.attrs)
            cur_ims = ImageStack(meta=ims_meta)
            ret.append(cur_ims)

            # add channels
            for chan_name, chan_dat in group.items():
                chan_meta = {'name': chan_name}
                chan_meta.update(chan_dat.attrs)
                cur_ims.add_image(np.array(chan_dat), chan_meta)

        if len(ret) == 0:
            raise ValueError('Empty file!')

        return ret

    def __enter__(self):
        return self

    def __exit__(self, *_):
        self.root.close()


def set_attr_gcimage(dset):
    """Set attributes for HDF5 dataset, to view it as gray scale in HDF5View
    """
    dset.attrs['CLASS'] = np.string_('IMAGE')
    dset.attrs['IMAGE_SUBCLASS'] = np.string_('IMAGE_GRAYSCALE')
    dset.attrs['IMAGE_VERSION'] = np.string_('1.2')
    # dset.attrs['IMAGE_MINMAXRANGE'] = np.array([0.0, 1.0], dtype=float)
    dset.attrs['IMAGE_WHITE_IS_ZERO'] = np.uint8(0)
