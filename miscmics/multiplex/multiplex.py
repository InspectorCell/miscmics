# -*- coding: utf-8 -*-
"""Handling image and metadata, make both accessible
"""
import platform
import time
import sys
import os
from typing import List, Iterable, Union, Callable
import copy
import logging
from pathlib import Path
import warnings
from itertools import chain

import imageio
from PIL import Image

import numpy as np

from ..util import find_matches, _match


_LOG = logging.getLogger(__name__)
# logging.captureWarnings(True)

class _NotProvided:
    # pylint: disable=too-few-public-methods
    pass


# TODO make me a dataclass?
class ImageStack():
    """ Stack of multiplexed images and their meta data

    Methods for manipulating the image data and metadata directly. Supposed
    be flexibel. Tries to derive metadata if none is available
    """

    def __init__(self, reader=None, meta: dict = None):
        self.data = []
        self._reader = reader

        if meta is None:
            self.meta = {}
        else:
            self.meta = copy.deepcopy(meta)

        try:
            uri = self.meta['uri']
            _LOG.warning('Found URI %s, might be overwritten!', str(uri))
        except KeyError:
            pass

    def __str__(self):
        ret = [f'{k}: {v}' for k, v in self.meta.items()]
        ret = '|'.join(ret)

        return f'<ImageStack - {ret}>'

    def copy(self):
        """ Creates a deep copy of the image stack

        Returns
        -------
        copy : ImageStack
            deep copy of this ImageStack instance
        
        Note
        ----
        syntactic sugar for 

        import copy

        ims = ImageStack()
        copy = copy.deepcopy(ims)
        """
        return copy.deepcopy(self)

    def astype(self, dtype):
        """ Creates a deep copy of the image stack

        Parameter
        ---------
        dtype : np.dtype
            Data type to cast every channel to

        Returns
        -------
        copy : ImageStack
            deep copy of this ImageStack instance with new dtype
            applied to all channels
        """
        ret = ImageStack()
        ret.data = [ch.astype(dtype) for ch in self.data]
        ret.meta = copy.deepcopy(self.meta)
        ret._reader = copy.deepcopy(self._reader)
        return ret

    def get_stack(self, metaqueries: List[dict],
            deep_copy: bool = False, single: bool = True) -> 'ImageStack':
        """Returns all data where matching metadata as ImageStack

        Parameters
        ------------
        metaqueries : List[dict]
            List of metaquery dicts as used in `ImageStack.find`. Each
            metaquery must unambigious return exactly on image.

        deep_copy : bool
            If `False` (default) the actual image and metadata is just
            referenced by the new ImageStack. If deep is `True`, all data
            including all metadata is copyied deeply into the new ImageStack.

        single : bool
            If True, raise an error if a meta query returns more than one
            channel. See also: ImageStack.find_multiple

        Returns
        ---------
        ndimg : ImageStack
            An ImageStack only containing the data as defined by the
            metaqueries. All metadata is copied.

        Raises
        --------
        ValueError
            If any of the metaqueries returns no or multiple images
            or if the dimension of the images for each channel are different
            or if the single channel images have more than one channel.
            -> ch.shape == (y, x) OR ch.shape == (y, x, 1)
        """
        channels = []

        for cond, mqu in zip(self.find_multiple(metaqueries), metaqueries):
            if single and len(cond) > 1:
                msg = f"'{str(mqu)}' returns {len(cond)} images but must be 1."
                raise ValueError(msg)
            channels += list(cond)

        if deep_copy:
            new_stack = self.fromarray(channels, meta=self.meta)

            for new_img, src_img in zip(new_stack.data, channels):
                new_img.meta.update(src_img.meta)
        else:
            new_stack = self.fromarray([])
            new_stack.data = channels

        new_stack.meta.update(self.meta)

        return new_stack

    @classmethod
    def fromarray(cls, data: Iterable, meta: dict = None):
        """Create a new ImageStack from an iterable

        Data will be fully copied!

        Parameters
        ------------
        data : Iterable
            Iterable to use for creation of new ImageStack
            Each item in the iterabel must be convertabel
            to an numpy array. The order of data in the
            data is preserved in ImageStack.data
            If the elements have a dict `meta` property, it will
            be copied

        meta : dict
            Meta data for the image stack. Metadata for channels
            can be set by iterating over ImageStack.data

        Returns
        ---------
        stack : ImageStack
            New image stack with `data` from

        Notes
        ------
        Iterates over all elements in data, interprets each as
        images and adds it as channel. If the elements in
        """

        new_stack = cls(meta=meta)

        for cur_ch in data:

            try:
                cur_meta = cur_ch.meta.copy()
            except AttributeError:
                cur_meta = {}

            new_stack.add_image(image_data=imageio.core.Array(cur_ch),
                                meta_data=cur_meta)

        return new_stack

    def get_ndimage(self, dtype=None) -> imageio.core.Array:
        """Returns all data where matching metadata as ndimage

        Returns
        ---------
        ndimg : imageio.core.Array
            An ndimage with shape (x, y, c) where x and y are the dimension
            and c ist the number of channels. In c = 0...n is the image data
            that matches the repective entriy in metaqueries[0...n]

            The metadata of the ndimage keeps the metaqueries and uri
        dtype : (default=None)
            Type of resulting ndimage. If None, using the same dtype as
            data is stored
        """

        chans = []
        nd_meta = []

        for cur_ch in self.data:
            cur_meta = copy.deepcopy(cur_ch.meta)

            if dtype is None:
                use_dtype = cur_ch.dtype
            else:
                use_dtype = dtype
            chans.append(cur_ch.astype(use_dtype))
            nd_meta.append(cur_meta)

        # create ndimg
        try:
            ret = imageio.core.Array(np.dstack(chans))
        except ValueError as err:
            lst = ', '.join(str(np.shape(ch)) for ch in chans)
            msg = f'Could not stack channels with shapes: {lst}'
            raise ValueError(msg) from err

        # add metadata
        ret.meta.update(copy.deepcopy(self.meta))
        ret.meta['channels'] = nd_meta

        return ret

    def find_multiple(self, metaqueries: List[dict]):
        """Returns all data where a matching metadata

        list of images generated from multiple queries. ordered according to
        queries. Synthactic sugar for
        `[ImageStack.find(mq, single=False) for mq in metaqueries]`

        Parameters
        ------------
        metaqueries : List[dict]
            List of metaquery dicts as used in `ImageStack.find`

        Returns
        ---------
        images : List[List[imageio.core.Array]]
            List of images found by the metaqueries. If an metaquery did
            not return any matches, an empty list is appended
        """
        # process the queries
        ret = []

        for metaq in metaqueries:
            chan = self.find(metaq, single=False)
            ret.append(chan)

        return ret

    def pop(self, metaqueries: List[dict]) -> List[imageio.core.Array]:
        """
        Finds multiple images from metaqueries, removes them in place
        and returns the removed images

        Parameters
        ------------
        metaqueries : List[dict]
            List of metaquery dicts as used in `ImageStack.find`

        Returns
        ---------
        images : List[imageio.core.Array]
            List of images found by the metaqueries. If an metaquery did
            not return any matches, an empty list is appended
        """
        pop_me = list(chain(*self.find_multiple(metaqueries)))
        new_dat = []

        for img in self.data:
            if any([_match(img.meta, pm.meta) for pm in pop_me]):
                continue
            new_dat.append(img)

        self.data = new_dat
        return pop_me

    def find(self, metaquery, single=True) -> List[imageio.core.Array]:
        """Returns all data where matching metadata

        Parameters
        ------------
        metaquery : Dictionary
            Abritary key and values. Each provided key is match
            againts data.meta. If the meta value matches the provided
            value

        single : bool (default True)
            If True, return only first match, else a list of all
            data matching the metaquery

        Returns
        ---------
        ret : List[imageio.core.Array]
            List of images that match the metaquere
        """

        return find_matches(metaquery, self.data, single)

    def find_one(self, **metakw) -> imageio.core.Array:
        """
        Find single channel that matches provided metakw

        Parameters
        ------------
        metakw : dict

        Returns
        ---------
        ret : imageio.core.Array
            Untupled output of ImageStack.find() or None

        Notes
        ------
        Syntastic sugar for ImageStack.find(metaquery=metakw, single=True)[0]
        """
        try:
            return self.find(metaquery=metakw, single=True)[0]
        except IndexError:
            return None

    def unique(self, keys: List[str] = None):
        """
        Returns all unique values in the metadata

        [TODO:description]

        Parameters
        ------------
        key : List[str] (default=None)
            Keys to to find unique values for. If None, a dict of sets is returned

        Returns
        ---------
        unique_meta : Dictionary
            Dictionary with key beeing the meta data field name and
            the value is the list of all unique values for the respective
            field

        Notes
        -------
        Does not assume homogenity of the data. Hence some unique meta
        data entries might be only applicable for part of the data stored
        -> uniquness only given on a per atrtibute level
        """

        if keys is None:
            keys = set([])

            for chan in self.data:
                keys = keys.union(chan.meta.keys())

        # setup return dict
        unique = {k: set([]) for k in keys}

        # Fill with unique values from channels
        for dat in self.data:
            for key, uni_set in unique.items():
                val = dat.meta.get(key, _NotProvided)

                # No meta entry for key in channel
                if val is _NotProvided:
                    continue

                try:
                    uni_set.add(val)
                except TypeError:
                    val = str(val)[:10]
                    warnings.warn(
                        f'Can not hash {key} convert to string'
                      + f' clipped string {val}')
                    uni_set.add(str(val))

        return unique

    # TODO read this uri, the reader will do the pasring of uri
    def read_dir(self, dir_path):
        """globs through dir_path and processes all images with
        image loader.
        """

        # if self.meta.get('uri') is None:
        #     self.meta['uri'] = str(dir_path)
        # else:
        #     LOG.error('Can only load once!')
        #     return

        # TODO askt the reader if exist, what are the components, etc.
        dir_path = Path(dir_path)

        if not dir_path.exists() or not dir_path.is_dir():
            # raise ValueError('Does not exist or is not a dir')
            _LOG.error('%s does not exist or is not a dir', str(dir_path))

            return

        self.meta['uri'] = generic_uri(str(dir_path.absolute()))

        # TODO for image in reader

        for file_path in dir_path.glob('*.*'):
            # TODO only return the image_data with metadata already attatched
            image_data, meta_data = self._reader(file_path)

            if image_data is None:
                _LOG.debug('Dismissing %s', str(file_path))

                continue

            # NOTE core functionality
            _LOG.debug('Adding %s', str(file_path))
            try:
                self.add_image(image_data, meta_data)
            except ValueError as err:
                _LOG.warning('Could not process the image %s', str(file_path))
                raise err

    # TODO write to this uri, use the writer
    # shoudl go into a writer class
    def write_dir(self, dir_path: Union[str, Path],
                  name_sheme: str='{name}.tif'):
        """Write image stack to folder

        Parameters
        ------------
        dir_path: Union[str, Path]
            Path to write the channels to
        name_sheme: str (default='{name}.tif')
            Naming schemes to generate file names for a channel
            can use any meta data from channel
        """
        dir_path = Path(dir_path)

        if not dir_path.exists() or not dir_path.is_dir():
            # raise ValueError('Does not exist or is not a dir')
            _LOG.error('%s does not exist or is not a dir', str(dir_path))

            return

        for img_data in self.data:
            img = Image.fromarray(img_data)
            fname = name_sheme.format(**img_data.meta)
            img_path = dir_path / fname

            if img_path.exists():
                _LOG.warning('%s used multiple times!', str(img_path))
            img.save(img_path)
            _LOG.debug('Saving %s', str(img_path))

    def add_image(self, image_data: Union[imageio.core.Array, np.ndarray],
                  meta_data: dict = None):
        """Handling tracking of meta data and image data

        ImageStack preserves order. The order images are added is reflected
        kept in ImageStack.data as well

        Parameters
        ------------
        image_data : Union[imageio.core.Array, np.ndarray]
            Image data array to be added.
        meta_data : dict (default=None)
            Dictionary with metadata for new image channel. Has precednece over
            metadata provided in imageio.core.Array if

        Raises
        --------
        ValueError
            If no/conflicting metadata is provided or if data is multi channel
        """
        # check if metadata is present as attibute or provided

        if meta_data is None:
            meta_data = {}
        try:
            img_meta = image_data.meta.copy()
        except AttributeError:
            img_meta = {}

        for key, val in meta_data.items():
            img_val = img_meta.get(key, _NotProvided)

            if img_val is _NotProvided:
                continue

            if not np.all(img_val == val):
                msg = ('conflicting metadata in ImageStack.add_image' +
                       f'image_data.meta[\'{key}\'] = {img_val}, differs from' +
                       f'meta_data[\'{key}\'] = {val}')
                raise ValueError(msg)

        img_meta.update(meta_data)

        # required metadata provided?

        if img_meta.get('name') is None:
            raise ValueError('Meta data must have at least a name key')

        # name unique in ImageStack
        names = [ch.meta['name'] for ch in self.data]

        if any(img_meta['name'] == name for name in names):
            name_exist = str(img_meta['name'])
            msg = (f'Image with name {name_exist} already exists in this stack')
            raise ValueError(msg)

        img_array = imageio.core.Array(image_data.squeeze())
        # data correct shape?

        if len(img_array.shape) != 2:
            msg = f'Image {str(img_array)} is not grayscale, with' +\
                  f' image shape {img_array.shape}'
            raise ValueError(msg)

        img_array.meta.update(img_meta)
        self.data.append(img_array)

        if self.meta.get('uri') is None:
            self.meta['uri'] = generic_uri()

    def crop(self, slc):
        """Clips all images with a slice in place

        Parameters
        ------------
        slc : slice
            Slice object
        """
        #TODO return new instance with data = np.view?
        self.data = [dat[slc] for dat in self.data]

    def update(self, new_data, **kwargs):
        """ Update date in channel

        Updates a channel inplace based on a search query

        Parameters
        ----------
        new_data : Union[imageio.core.Array, np.ndarray]
            New image data to replace/update the matched channel
        **kwargs
            Keywords used directly in ImageStack.find to match the
            channel to update. See ImageStack.find for more details

        Raises
        ------
        ValueError
            - If supplied kwargs match multiple channels or no channel
            - If new_data can not be broadcated to channel data
        """
        new_data = new_data.squeeze()

        matches = self.find(kwargs, single=False)
        if not matches:
            raise ValueError(
                f"No matched channel with '{kwargs}'")
        elif len(matches) > 1:
            raise ValueError(
                f"Ambiguous '{kwargs}' matches {len(matches)} channels (> 1)")
        ch, = matches

        try:
            # inplace update
            ch[:] = new_data
            # reference update
            # for i in range(len(self.data)):
            #     if ch is self.data:
            #         self.data[i] = new_data
            #         break
        except ValueError as err:
            raise ValueError('Invalid shape for new_data') from err

class ImageReader():
    """ Baseclass for reader

    basic class for loading images. defines the per image
    data format and provides metadata parsing
    """

    def __call__(self, uri):
        """Called by ImageStack. Must return an scipy.ndimage/ndarray
        containing the image data and a meta data dict
        """
        image_data = np.array([])
        meta_data = {}
        _LOG.warning('Using a base class here... better be debugging...')

        return image_data, meta_data

    @staticmethod
    def read_image(uri, meta=None):
        """Reads the image data from the uri and returns
        the image data in an array
        """
        # somehow very slow
        # return imageio.imread(uri, format='tif')

        # fast reading of image files +
        # imageio sublclass of ndarray with meta data
        img = Image.open(uri)

        return imageio.core.Array(np.array(img), meta)

    @staticmethod
    def sanetize_meta(meta):
        """sanetizes the meta dict a bit

        removes trailing white spaces and tries to find and convert numeric
        values to floats
        """
        ret = {}

        for key, val in meta.items():
            if val is None:
                val = 'N/A'
            val = val.strip()
            try:
                val = float(val)
            except ValueError:
                pass
            ret[key] = val

        return ret

def generic_uri(res_identifier: str = None, sep: str = '||') -> str:
    """Creates a generic URI gathering as much information as possible
    """
    cwd = Path(os.getcwd()).expanduser().absolute()
    try:
        script = Path(sys.argv[0])

        if not script.is_absolute():
            script = (cwd / script).resolve()

        if not script.exists():
            script = ''
        script = f'PYS{script}'
    except (ValueError, TypeError):
        script = 'PYS?'
    cwd = f'CWD{cwd}'

    try:
        exe = Path(sys.executable).expanduser().absolute()

        if not exe.exists():
            exe = 'EXE?'
        else:
            exe = f'EXE{exe}'
    except (ValueError, TypeError):
        exe = 'EXE?'

    host = platform.node()
    lctime = time.localtime()
    tstamp = (f'{lctime.tm_year}.{lctime.tm_mon}.{lctime.tm_mday}' +
              f'-{lctime.tm_hour}:{lctime.tm_min}')

    generic_uri_str = f'<{tstamp}|{host}>{sep}{exe}{sep}{cwd}{sep}{script}'

    if res_identifier is not None:
        generic_uri_str += f'{sep}RES{res_identifier}'

    return generic_uri_str

def apply_channelwise(image_stack: ImageStack, func: Callable,
                      inplace: bool = False, skip: List[dict] = None):
    """Applies function channelwise

    Parameters
    ------------
    image_stack : ImageStack
        ImageStack instance to normalize
    func : Callabel
        Function/Callable applied for each channels
    inplace : bool
        If False (default), a copy is returned. If True,
        mapping/changes are made inplace
    skip : List[dict]
        List of metadata dicts. If an image array matches,
        it is skipped

    Returns
    ---------
    mapped : ImageStack
        Mapping of the input ImageStack

    Notes
    ------
    If no extra caution is taken, meta data might be lost when
    changes are made inplace
    """
    if skip is None:
        skip = []

    def _to_skip(chan):
        return any((_match(mq, chan.meta) for mq in skip))

    mapped_channels = []
    for chan in image_stack.data:
        if not _to_skip(chan):
            _LOG.debug(f'Mapping {chan.meta}')
            new_chan = func(chan)
        else:
            _LOG.debug(f'Skipping {chan.meta}')
            new_chan = chan

        if not hasattr(new_chan, 'meta'):
            raise ValueError('Meta was lost during mapping, or func not'+
                                 'returning valid channel data')
        mapped_channels.append(new_chan)

    if inplace:
        mapped_imagestack = image_stack
        mapped_imagestack.data = mapped_channels
    else:
        mapped_imagestack = ImageStack()
        mapped_imagestack.meta = copy.deepcopy(image_stack.meta)
        for chan in mapped_channels:
            mapped_imagestack.add_image(chan, copy.deepcopy(chan.meta))

    return mapped_imagestack
