"""Process images in an filesystem directory
"""

import re

from .multiplex import ImageReader


class ProcessedReader(ImageReader):
    """Read image data using abritary regex patter
    for extraction of metadata
    """

    def __init__(self, pattern=None):
        """
        Parameters
        ----------
        pattern : str
            regex pattern

        Notess
        ------
        Named groups define fully, which metadata is used/stored extracted
        """
        # to filename matcher

        if pattern is None:
            pattern = ('^(?P<cycle>[0-9]{3})_(?P<name>[a-z0-9]*)([V_ ][0-9]' +
                       '*.*wix ?(?P<exp>[0-9]*)|__16bit).*(?P<ext>.tiff?)$')

        self._matcher = re.compile(pattern, re.IGNORECASE)

    def __call__(self, uri):

        meta = self.get_meta(uri)

        if meta is None:
            return None, None

        meta = self.sanetize_meta(meta)

        dat = self.read_image(uri, meta)

        return dat, meta

    def get_meta(self, uri):
        """Extract metadata from URI <- Filename
        """
        match = self._matcher.match(uri.name)
        try:
            meta = match.groupdict()
            meta['filename'] = uri.name
            return meta
        except AttributeError:
            return None
