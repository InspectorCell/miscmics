from typing import Tuple


__all__ = ['is_out_of_bound']


def is_out_of_bound(slc: Tuple[slice],
                    shape: Tuple[int]) -> bool:
  """
  Check if slice is within shape

  Parameters
  ----------
  slc : Tuple[slice]
     Slices to check if in bound
  shape : Tuple[int]
     Shape of where to cut out from later

  Returns
  -------
  bool:
      True if out of bound
  """
    
  invalid = False

  for dim_slc, stack_dim in zip(slc, shape):
    start_oob = not (0 <= dim_slc.start < stack_dim)
    stop_oob = not (0 <= dim_slc.start < stack_dim)
    null = dim_slc.stop - dim_slc.start <= 0
    invalid = start_oob or stop_oob or null
    if invalid:
      break

  return invalid

