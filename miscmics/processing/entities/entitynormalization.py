""" Processing, normalize change entities
"""
from typing import Dict
from enum import Enum, auto as autonum

from ...entities import Entity


class _Resolve(Enum):
    mean = autonum()
    err = autonum()
    replace = autonum()


def normalize_scalarkeys(ent: Entity, mapping: Dict[str, str],
                         resolve: str = 'err') -> Entity:
    """
    Rewrites  scalar keys according to mapping

    Changes the scalar keys of the entity _inplace_. Returns the
    changed Entity.

    Parameters
    ----------
    ent : Entity
        Entity with scalars to update
    mapping : Dict[str, str]
        Mapping used to update the scalar keys in an Entity. Mapping key will
        changed to value and key will be removed
    resolve : str
        how to resolve conflicting assignments, if both key and value exist,
        but have different scalar values. Must be either 'mean'
        (take mean of all scalar values), 'err' (raise ValueError, default) or
        'replace' ('replace with value for normalized key')

    Raises
    ------
    ValueError
        If both, unnormalized key and normalized key already exist, have
        different values and resolve is 'err' (default)

    Returns
    -------
    Entity:
        Same Entity as input parameter. Changed inplace
    """
    
    try:
        _resolve = _Resolve[resolve]
    except KeyError:
        valid = ', '.join([f"'{mn}'" for mn in _Resolve._member_names_])
        raise ValueError(
            f'Resolve must be any of {valid}')

    for from_key, to_key in mapping.items():
        
        if not from_key in ent.scalars:
            continue
        
        from_value = ent.scalars[from_key]
        # to value defaults to cur_value if not exist
        # -> no conflivt
        to_value = ent.scalars.get(to_key, from_value)
        new_value = (from_value + to_value) * 0.5

        if new_value != from_value: 
            if _resolve == _Resolve.err:
                raise ValueError(
                    f"Scalar value {from_key}={from_value} (unnormalized) is " +
                    f"different from {to_key}={to_value} (normalized)")
            elif _resolve == _Resolve.replace:
                new_value = to_value
        
        # update ent scalars
        ent.scalars[to_key] = new_value
        ent.scalars.pop(from_key)

    return ent
