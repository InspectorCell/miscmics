""" Util functions to manipulate Entities and EntityLedgers
"""
# built-in
from typing import List

# project
from ...entities.ledger import EntityLedger
from ...entities.entity import EntityType
from ...entities.imageentity import ImageEntity

# external
import numpy as np
import cv2


__all__ = ['offset_contour', 'offset_ledger', 'simplify_contour']


def offset_ledger(ledger: EntityLedger, offset: np.ndarray) -> List[np.ndarray]:
    """Offset entity contours inplace

    Parameters
    ----------
    ledger : EntityLedger
        offset all contours for all entities in ledger
    offset : np.ndarray
        Offset appplied to contour -> contour - offset

    Returns
    ----------
    ledger : EntityType
        Same as input, after inplace offsetting of all entities
    """

    offset = np.array(offset).reshape(1, 2)
    for ent in ledger.entities.values():
        if ent.etype != EntityType.Historic:
            ent.update_contour(offset_contour(ent.contour, offset))
            ent.update_contour(ent.contour)

    return ledger

def offset_contour(contour: List[np.ndarray], offset: np.ndarray) -> List[np.ndarray]:
    """Simplify a contour, if possible

    Just removing doublicates

    Parameters
    ----------
    contour : List[np.ndarray]
        Contour to be simplified. Will remove doublicates
    offset : np.ndarray
        Offset appplied to contour -> contour - offset

    Returns
    ----------
    contour : List[np.ndarray]
        Offsetted contour
    """
    offset_contour = []
    for shape in contour:
        offset_contour.append(shape - offset)
    return offset_contour


def simplify_contour(contour: List[np.ndarray]) -> List[np.ndarray]:
    """Simplify a contour, if possible

    Just removing doublicates

    Parameters
    ----------
    contour : List[np.ndarray]
        Contour to be simplified. Will remove doublicates

    Returns
    ----------
    contour : List[np.ndarray]
        Now hopfully simplified contour
    """

    simplified = []
    for shape in contour:
        simple_shape = []
        last_pt = None, None
        for cur_pt in shape:
            if cur_pt[0] != last_pt[0] or cur_pt[1] != last_pt[1]:
                simple_shape.append(cur_pt)
            last_pt = cur_pt
        simplified.append(simple_shape)
    return simplified


