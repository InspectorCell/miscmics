"""Extract quantities from ImageStacks and Entities
"""
from typing import Dict, Callable, Iterable, Tuple, Union, List, Set
import logging
import warnings
import copy
from itertools import product

import numpy as np

# pylint: disable=no-name-in-module, c-extension-no-member
import cv2
from scipy.spatial import Voronoi, qhull
# pylint: enable=no-name-in-module

from ...multiplex import ImageStack
from ...entities.ledger import EntityLedger
from ...entities import ImageEntity, EntityType, Entity, EntityFactory
from .. import is_out_of_bound
from .manipulate import simplify_contour


__all__ = ['crop_to_shape', 'extract_features', 'generate_segment_sliceouts',
           'get_annotation', 'get_box_center', 'get_mass_center',
           'get_sliced_mask', 'mask_to_contour', 'segment_to_slice',
           'within_image', 'annotate_neighbours', 'create_from_pixmap']


def mask_to_contour(mask_slice: List[slice],
                    mask: np.ndarray) -> List[np.ndarray]:
    """Generates a contour for a mask, ofsetted by an slice

    Parameters
    ------------
    mask_slice : List[slice]
        "Slice that cut out the mask", used to calculate the contout offset
        As e.g. returned by :class:`numpy.lib.index_tricks.IndexExpressio`.
        The mask and mask_slice must have corresponding shapes

    mask : np.ndarray
        Boolean mask, selecting the pixel within mask_slice that are inside
        the contour

    Returns
    --------
    contour : List[np.ndarray]
        Contour of the mask
    """

    slice_shape = []
    for slc in mask_slice:
        try:
            slice_shape.append(slc.stop - slc.start // slc.step)
        except TypeError:
            slice_shape.append(slc.stop - slc.start)

    if tuple(slice_shape) != mask.shape:
        raise ValueError(f'Mask slice {tuple(slice_shape)} and mask ' +
                f'{mask.shape} do not match')

    # compress verticals and horizontals
    method = cv2.CHAIN_APPROX_SIMPLE

    # retrive tree hirachy
    # mode = cv2.RETR_EXTERNAL
    mode = cv2.RETR_LIST

    contour_data = cv2.findContours(
            mask.astype(np.uint8), mode, method)
    try:
        # opencv version 3
        _, contour, _ = contour_data
    except ValueError:
        # opencv version 2
        contour, _ = contour_data

    contour = [shape.squeeze() for shape in contour]

    if mask_slice is None:
        return contour

    shifted_contour = []
    for shape in contour:
        row_of, col_of = mask_slice

        try:
            shape[:, 0] += col_of.start
            shape[:, 1] += row_of.start
        except IndexError:
            # catch exception if contour is just a point
            shape = shape.reshape(1, 2)
            shape[:, 0] += col_of.start
            shape[:, 1] += row_of.start

        shifted_contour.append(shape)

    return shifted_contour

def get_sliced_mask(arr, value, seek=1000):
    """Find the slice and bool mask for value in array. Search in
    in around seek indices around first occurence of value in array

    Returns
    --------
    value_slice : tuple of slices
        slicecing objects, locating bool mask in the array arr

    mask : boolean ndarray mask
        boolian mask, masking all values in arr

    Notes
    ------
    slice is inclusive, thus arr[slice].shape == mask.shape

    seek defines a view on arry to look for a slice. Given ath the index (n, m)
    is the first occurence of value in arr, then the mask will be genarted only
    for the view arr[n - seek:n + seek, m - seek:m + seek]
    """
    # find index of first occurence
    first_index = np.argmax(arr == value)
    first_row, first_col = np.unravel_index(first_index, arr.shape)
    arr_rows, arr_cols = arr.shape

    # get arr offset, and serach window
    view_row0 = max(first_row - seek, 0)
    view_col0 = max(first_col - seek, 0)
    view_row1 = min(first_row + seek, arr_rows)
    view_col1 = min(first_col + seek, arr_cols)
    window = arr[view_row0:view_row1, view_col0:view_col1]

    window = window == value
    rows = np.any(window, axis=1)
    cols = np.any(window, axis=0)
    win_rows, win_cols = window.shape
    rmin, rmax = np.argmax(rows), win_rows - np.argmax(rows[::-1]) - 1
    cmin, cmax = np.argmax(cols), win_cols - np.argmax(cols[::-1]) - 1

    mask = window[rmin:rmax+1, cmin:cmax+1]
    mask_rows, mask_cols = mask.shape

    row_offset = view_row0 + rmin
    col_offset = view_col0 + cmin

    value_slice = np.s_[row_offset:row_offset + mask_rows,
            col_offset:col_offset + mask_cols]

    return value_slice, mask

def get_mass_center(entity: ImageEntity, keepdims=True) -> np.ndarray:
    """
    Returns the mass center of an ImageEntity contour

    This is equal to the ImageEntity.center property (which is the mass center)

    Parameters
    ----------
    entity : ImageEntity
        Entity for which the center is returned
    keepdims : bool
        If true (default) the output is (1, 2) else
        dims are reduced to (2,)

    Returns
    -------
    np.ndarray:
        Array of shape (1, 2) or (2,) which are the mass
        center of the contour
    """
    if keepdims:
        return entity.center.reshape(1, 2)

    return entity.center.reshape(2)

def get_box_center(ent: ImageEntity):
    """Calculates center pixel of Entity segment
    """
    sl0, sl1 = ent.slc
    ret = np.array([(0.5*sl.stop + 0.5*sl.start) for sl in (sl0, sl1)])

    return ret

def get_annotation(ent: ImageEntity, names: Iterable[str]):
    """looks up names in order from ent.scalars and returns
    list of anotation values

    If ent has no scalar assigned for name, 0 is returned
    else int(val > 1)
    """
    ret = []

    for mname in names:
        val = ent.scalars.get(mname, 0)
        ret.append(int(val > 0))

    return ret

def within_image(slc: slice, img_shape: Tuple[int]) -> bool:
    """Returns true is slc is in bounds of image shape
    """
    diffs = [cur_slc.stop - cur_slc.start for cur_slc in slc]
    bdiffs = []

    for cur_slc, shape_bound in zip(slc, img_shape):
        bstop = np.min([cur_slc.stop, shape_bound])
        bstart = np.max([cur_slc.start, 0])
        bdiffs.append(bstop - bstart)

    return diffs == bdiffs

def segment_to_slice(ent: ImageEntity, shape: Tuple[int],
        center_func=get_mass_center) -> Tuple[slice]:
    """
    Crops out ImageEntities around the center returned by center_func

    Parameters
    ----------
    ent : ImageEntity
    Entity around witch the slice is calucalted
    shape : Tuple[int]
    final shape
    center_func : [TODO:type]
    Function to calculate center

    Returns
    -------
    Tuple[slice]:
    Sliceobject tha cuts out the entity around the center
    """
    center = np.round(center_func(ent)).astype(int).squeeze()
    rows, cols = shape
    ro0 = center[1] - rows // 2
    co0 = center[0] - cols // 2
    ro1 = ro0 + rows
    co1 = co0 + cols
    return np.s_[ro0:ro1, co0:co1]

def crop_to_shape(*args, **kwargs):
    logging.getLogger(__name__).warning(
            "Depreciated name 'crop_to_shape', use 'fit_to_shape' instead ")
    return fit_to_shape(*args, **kwargs)

def fit_ent_shape(ent, shape):
    """Syntacitc sugar for fit_to_shape(ent.slc, ent.mask, shape)
    """
    return fit_to_shape(slc=ent.slc, mask=ent.mask, shape=shape)

def fit_to_shape(slc: Tuple[slice], mask: np.ndarray,
        shape: Tuple[int]) -> Tuple[slice, np.ndarray]:
    """Crop or extend slice and mask to given shape

    Similar to gement to slice, but also updates the mask
    """

    if shape is None:
        raise ValueError('Shape must be a tuple of int, not None')

    new_slice = ()
    new_mask = mask.copy()
    slc_shape = [sl.stop - sl.start for sl in slc]

    # trivial
    if tuple(slc_shape) == shape:
        yslc, xslc = slc

        return np.s_[yslc.start:yslc.stop, xslc.start:xslc.stop], mask.copy()

    for i in range(2):
        cur_len = slc[i].stop - slc[i].start
        to_len = shape[i]
        diff = cur_len - to_len
        ddiv, dmod = divmod(abs(diff), 2)
        # do the slice

        if diff > 0: # segment larger than requested shape
            change_left = ddiv + dmod
        elif diff < 0: # segment smaller than requested shape
            change_left = -(ddiv + dmod)
        elif diff == 0:
            change_left = 0
        new_start = slc[i].start + change_left
        new_stop = new_start + to_len
        new_slice = new_slice + (slice(new_start, new_stop, None),)

        # do the mask
        if diff > 0: # segment larger than requested shape
            change_left = ddiv + dmod
            new_mask_slc = np.s_[change_left:change_left+to_len, :]

            if i == 1:
                new_mask_slc = new_mask_slc[::-1]
        elif diff < 0: # segment smaller than requested shape
            change_left = ddiv + dmod
            change_right = to_len - (cur_len + change_left)
            new_mask_pad = [[change_left, change_right], [0, 0]]

            if i == 1:
                new_mask_pad = new_mask_pad[::-1]
        elif diff == 0:
            change_left = 0

        if diff > 0:
            new_mask = new_mask[new_mask_slc]
        elif diff < 0:
            new_mask = np.pad(new_mask, pad_width=new_mask_pad, mode='constant')

    return new_slice, new_mask

def generate_segment_sliceouts(ledger: EntityLedger, stack: ImageStack,
        mask: bool = False, oob_tag: str = 'cropped',
        to_shape: Union[Tuple[int], None] = None
        ) -> Tuple[Entity, Union[np.ndarray, None]]:
    """
    Parameters
    ----------
    ledger : EntityLedger
        Ledger holding entities, for which features are to be
    stack : ImageStack
        Image source from which is cropped to boundingbox of entity
    mask : bool (default=False)
        Apply the entity mask. If `True` return a `(i, t)` shaped array where
        `i` indexes the pixel and `c` the channel. All spatial information
        of the pixels is lost.
        If `False` (default) return the slice with the shape `(x, y, c)` where
        `x` and `y` are the spatial pixel dimension and `c` is the channel
    oob_tag : str (default='crop')
        If a entity is out of bounds, the entity is tagged with the oob_tag
    to_shape : Union[Tuple[int], None] (default=None)
        A tuple of int defining the output shape (x, y). Extends or shrinks
        the sliceout area.

    Returns
    -------
    ent, crop : Tuple[Entity, Union[np.ndarray, None]]
        Entity and the corresponding crop from ndstack. If entitiy is out of
        bounds, the oob tag is added to entity and crop is None

    Notes
    ------
    As side effect adds tag to the entities in ledgers, if these are around
    skipps historic entities
    """
    ndstack = stack.get_ndimage()
    def _slice_gen(ndstack):
        for ent in ledger.entities.values():

            if ent.etype == EntityType.Historic:
                continue

            if to_shape is not None:
                use_slc, use_mask = crop_to_shape(ent.slc, ent.mask,
                        shape=to_shape)
            else:
                use_slc, use_mask = ent.slc, ent.mask

            invalid = is_out_of_bound(use_slc, ndstack.shape[:-1])

            if invalid:
                ent.tags.add(oob_tag)
                yield ent, None

                continue

            segment_crop = ndstack[use_slc]

            if mask:
                yield ent, segment_crop[use_mask]
            else:
                yield ent, segment_crop

    return _slice_gen(ndstack)

def extract_features(ledger: EntityLedger,  # pylint: disable=too-many-locals
        stack: ImageStack, key: str, raise_oob: str = 'ignore',
        oob_tag: str = 'out-of-bounds',
        feature_functions: Dict[str, Callable] = None):
    """Extract features of Entities in EntityLedger on ImageStack

    Quantifies image featuers found in ImageStack for segments in EntityLedger.
    For each segment the ImageStack is sliced an all pixels not belonging to the
    respective segment are masked. The resulting pixel, which are all pixel
    inside the segment are summarized/quantified using the feature functions

    Parameters
    ------------
    ledger : EntityLedger
        Ledger holding entities, for which features are to be
    stack : ImageStack
        Image source from which the features are supposed to be extracted from
    key : str
        Metadata key to group the image in image stack by. Features are
        extracted for every image in `stack` having metadata with `key`
        The fusion of '{key}_{value}' will be used as scalar key in each entity
    raise_oob : str (default='ignore')
        Controlls how entities out of bounds are handled. Either 'ignore' or
        'raise'. If ignored, entities will be tagged: 'out-of-bounds'
    oob_tag : str (default='out-of-bounds')
        This tag is set if `raise_oob` is not 'raise'
    feature_functions : Dict[str, Callable] (default=None)
        List of callables. Must take as arguments pixels, the ent and an ndarray
        Each function must return a float value
        If 'None' default fetures are extracted.

    Raises
    -------
    KeyError:
        If the key is in no meta data in ImageStack at all
    ValueError:
        Invalid parameters are passed
    IndexError:
        If an entity mask is out of bounds and `raise_oob` is 'raise'

    Notes
    -----
    Changes entities in ledger in place. if 'zero'
    """
    # pylint: disable=too-many-arguments
    log = logging.getLogger('extract_features')

    unique_kvals = stack.unique()[key]
    stack = stack.get_stack([{key: uni} for uni in unique_kvals])

    # raise key error if the key is not present at all in image stack

    if not key in stack.unique().keys():
        raise KeyError(f'{key} not present in stack!')

    if feature_functions is None:
        feature_functions = dict(
                mean=lambda px, *args: np.mean(px),
                sum=lambda px, *args: np.sum(px))

    if raise_oob not in ('ignore', 'raise'):
        raise ValueError("raise_oob must be 'ignore', 'crop' or 'raise'")

    gen = generate_segment_sliceouts(ledger, stack, mask=False, oob_tag=oob_tag)

    # extract features
    oob_ents = []
    ent_scalars = {}

    for ent, crop in gen:
        if crop is None:
            oob_ents.append(ent)
            continue

        ent_scalars[ent.eid] = scalars = {}

        for ch_idx in range(crop.shape[-1]):
            # channel metafor cur
            ch_meta = crop.meta['channels'][ch_idx]
            ch_px = crop[..., ch_idx]

            for feat_name, func in feature_functions.items():
                scalar_key = f'{key}_{ch_meta[key]}_{feat_name}'
                val = func(ch_px[ent.mask], ent, ch_px)

                if np.isnan(val) or np.isinf(val):
                    log.error('Invalid value for entity %s: %s', ent,
                            scalar_key)
                    val = np.nan
                scalars[scalar_key] = val

    oob_eids = ', '.join([str(oobe.eid) for oobe in oob_ents])
    msg = f'There are {len(oob_ents)} entities out of bound: {oob_eids}'

    if oob_ents and raise_oob == 'raise':
        raise IndexError(msg)
    # only warn now
    if oob_ents:
        warnings.warn(msg)

    for eid, sc_val in ent_scalars.items():
        ledger.entities[eid].scalars.update(sc_val)

def find_neighbours(entity_index, vor_tessalation) -> Set[int]:
    """ Utility function to detect all neighbouring indices
    for index point in tessalation

    Parameters
    ----------
    entity_index : int
        Index of entity in voronoid points
    vor_tessalation : Voronoid
        Voronoid tessalations
    """
    region_index = vor_tessalation.point_region[entity_index]

    # OOB region
    if region_index == -1:
        return set([])

    # only non-negative ones
    vertex_indices = set([
        i for i in vor_tessalation.regions[region_index] if i > -1])

    # add all ridges indices, that have intersecting vertices with
    # vertices sorounding the Voronoid region the indext entity lies in
    ridge_indices = []
    for r_idx, rv_idx in enumerate(vor_tessalation.ridge_vertices):
        if vertex_indices.intersection(rv_idx):
            ridge_indices.append(r_idx)

    # neighbouring points are separated by the found ridges
    neighbours = set(np.unique(
        vor_tessalation.ridge_points[ridge_indices]))
    neighbours.remove(entity_index)

    return neighbours

def annotate_neighbours(ledger: EntityLedger, max_dist: float,
                        inplace: bool = True) -> EntityLedger:
    """ Determine the neighbours of entity within EntityLedger

    Uses Voronoid tessalation to determine neighbourhod between
    two ImageEntities within an EntityLedger. The Entities get an
    annotation into generics.

    Raises
    ------
    ValueError
        If ledger has less than three entities, or Voronoids can not be created

    Notes
    -----
    In order to store this information, in a json file, the neighbour
    One could do numpy formbuffer/tobytes. The resulting shape in generics
    will be an array of the shape (N x m) arrays. Where N is the number
    of neighbours and m is dependend on array dtype.
    """
    if not inplace:
        ledger = copy.deepcopy(ledger)

    # ordered list, which is used to map voronoid regions to
    # to corresponding entities. Raise ValueError to prevent
    # Voronoi to go baszonkas if there are not enough entities
    # in the ledger
    entity_list = list(ledger.entities.values())

    if len(entity_list) < 3:
        raise ValueError(
            f'At least 3 entities must be in ledger (is {len(entity_list)})')

    centers = np.array([e.center for e in entity_list])

    try:
        vor_tessalation = Voronoi(centers)
    except qhull.QhullError as err:
        msg = 'Could not perform Voronoi tessalation, all entities in a line?'
        raise ValueError(msg) from err

    # import matplotlib.pyplot as plt
    # import IPython as ip
    # from scipy.spatial import voronoi_plot_2d
    # ip.embed()

    for for_idx, for_ent in enumerate(entity_list):
        # sanity check
        assert np.allclose(for_ent.center, vor_tessalation.points[for_idx])

        neighbours = for_ent.generic['neighbours'] = []
        for neigh_idx in find_neighbours(for_idx, vor_tessalation):
            neigh_ent = entity_list[neigh_idx]

            # skip if not meeting max distance criteria
            if np.linalg.norm(
                np.array(neigh_ent.center)-np.array(for_ent.center)) > max_dist:
                continue

            neighbours.append(neigh_ent.eid)

    return ledger


def create_from_pixmap(pixelmap: np.ndarray,
                       background_value=0) -> EntityLedger:
    """
    Creates entity ledger and fills it with entities from pixelmap

    Assumes each pixel is assigned to exactly on entity in the image. The pixels
    are aggregated into contours.

    Parameters
    ----------
    pixelmap : np.ndarray
        Numpy array with pixel values, where pixel with the same value belong to
        a single ImageEntity
    background_value : int
        Pixels with this value will not be used for entities

    Returns
    -------
    EntityLedger
        Ledger with ImageEntities
    """
    # ret_polygons = []
    fac = EntityFactory()

    valid_values = np.unique(pixelmap.ravel())
    valid_values = valid_values[valid_values != background_value]

    for cur_value in valid_values:
        mask_slice, mask = get_sliced_mask(pixelmap, cur_value)

        contour = simplify_contour(mask_to_contour(mask_slice, mask))
        # ret_polygons.append(contour)
        ent = fac.create_entity(cls=ImageEntity, contour=contour)
        ent.scalars['object_id'] = cur_value

    return fac.ledger
