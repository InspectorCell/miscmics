""" Register ImageStacks using absolute channel-wise values
"""
import numpy as np
import imageio
from pathlib import Path


def correct_imagestack(ims, channel_offsets):
    """ Inplace correction of ImageStack

    ims: ImageStack
    channel_offsets: Dict[str, Iterable[int]]
        OutoutfileFilepath, ImageStack
    """

    # for marker_name, offsets in channel_offsets.items():
    re_registered = []
    for ch in ims.data:
        dy, dx = channel_offsets.get(ch.meta['marker'], [0, 0])
        if dy == dx == 0:
            reg_chan = ch.copy()
        else:
            reg_chan = np.roll(np.roll(ch, dy, axis=0), dx, axis=1)
        reg_chan.meta['shift_yx'] = [dy, dx]
        re_registered.append(reg_chan)
    ims.data = re_registered

def create_registration_diffimages(image_stacks, out_dir, suffix):
    """ Create diffimages for each channel if has shift_yx meta key

    image_stacks: Dict[str, ImageStach]
    """

    for ims_diff_dir, ims in image_stacks.items():
        ims_diff_dir = Path(ims_diff_dir)
        for chan in ims.data:
            dydx = chan.meta.get('shift_yx', None)
            if dydx is None:
                continue
            dy, dx = tuple(dydx)
            if dy == 0 and dx == 0:
                continue
            
            # convert to 8 bit if not tiff
            chan = chan.astype(float) / chan.max()
            if suffix.lower().startswith('.tif'):
                chan = (chan * 0xffff).astype(np.uint16)
            else:
                chan = (chan * 0xff).astype(np.uint8)
            
            dest_file = out_dir / ims_diff_dir / chan.meta['name']
            dest_file.parent.mkdir(parents=True, exist_ok=True)

            dy, dx = dydx
            diff = np.roll(np.roll(chan, -dy, axis=0), -dx, axis=1) - chan
            imageio.imwrite(dest_file.with_suffix(suffix), diff)
