"""Normalizing channels in an imagestack
"""
from typing import Union, Callable

import numpy as np
import scipy.stats as stats
from scipy.special import expit
from sklearn.mixture import BayesianGaussianMixture

from ...multiplex.multiplex import ImageStack, apply_channelwise


def mode_value(hist: np.ndarray, bins: np.ndarray) -> float:
    """
    Find mode in histogram of array

    Parameters
    ----------
    hist : np.ndarray
        Value counts where to find the mode
    bins : np.ndarray
        Bin values to the counts / hists

    Returns
    -------
    float:
        Mode bin value for hist
    """
    mode_idx = np.argmax(hist == hist.max())
    mode_bin_cent = ((bins[1:] + bins[:-1]) / 2)[mode_idx]
    return float(mode_bin_cent)

# Maybe a nice class for namespacing or spearate module?
# default transforms
__TRANSFORMS = {
    'log2': lambda ch: np.log2(ch.ravel() + 1), # to bits
    'log10': lambda ch: np.log10(ch.ravel() + 1), # to db
    None: lambda ch: ch.ravel(), # identity
}

# default center functions
__CENTERING = {
    'mode': lambda px, hist, bins: px - mode_value(hist, bins),
    None: lambda *args: args, # identity
}

# default scaling functions
__SCALING = {
    'std': lambda px: px / px.std(),
    '2std': lambda px: px / (2 * px.std()),
    'max': lambda px: px / px.max(),
    None: lambda *args: args, # identity
}

# default suqeezy functions
__SQUEEZE = {
    'tanh': np.tanh,
    'sigmoid': expit,
    None: lambda *args: args, # identity
}

def __fetch_and_apply(lut_dict: dict, key: Union[str, Callable], *args):
    """looks up key and returns the application
    of the key mapping to *args. no mapping for key
    is found in `lut_dict` return application of key
    to *args

    Raises
    ------
    Any: All exception the Callable might raise
    """

    # check if key for dict is provided,
    # or a Callable
    try:
        func = lut_dict.get(key, key)
    except TypeError as exc:
        if 'unhashable' in str(exc):
            func = key
        else:
            # shuld never happen
            raise RuntimeError(
                'An type error occured during an dictionary lookup') from exc
    return func(*args)

def normalize_channel(
    channel: np.ndarray, transform: Union[str, Callable, None] = 'log2',
    center: Union[str, Callable, None] = 'mode',
    scale: Union[str, Callable, None] = 'std', bincount: int = 100,
    squeeze: str = None, quantile: float = 1.0):
    """Normalize imagestack by histogram centering and scaling and transform

    Parameter
    ---------
    channel: np.ndarray
        raw data to preprocess in channel
    transform: Union[str, Callable] (default='log2')
        log transform. Must be None, string or a callable that returns
        raveld 1d array
    center: str (default='mode')
        Definiton of center for centering after transform
    scale: str (default='std')
        Scaling of the data after centering
    squeeze : Union[str, callable]
        Squeezing function after scaling
    quantile : float (default=1.0)
        Limit the search of the mode in the qth quantile
    """
    # pylint: disable=too-many-arguments
    # lookup transfrom, if isn't predefined, (or hashable)
    # interpret transform as callable to be applied to
    # pixel values
    pixels = __fetch_and_apply(__TRANSFORMS, transform, channel)
    # calculate hist and bins
    firstq_m = pixels <= np.quantile(pixels, quantile)
    hist, bins = np.histogram(pixels[firstq_m], bins=bincount)
    # centering
    pixels = __fetch_and_apply(__CENTERING, center, pixels, hist, bins)
    # scaling
    pixels = __fetch_and_apply(__SCALING, scale, pixels)
    # squish everything
    pixels = __fetch_and_apply(__SQUEEZE, squeeze, pixels)
    return pixels.reshape(channel.shape)

def normalize_stack(image_stack: ImageStack, norm: Callable, **kwargs):
    """Channelwise normalization

    Apply normalization defined in `normalize_channel` to
    every channel in `image_stack`

    Parameters
    ----------
    image_stack: ImageStack
        ImageStack instance to normalize
    **kwargs: Dict[str, Any]
        Parameter passed to `miscmics.processing.normalize_channel`

    Note
    ----
    Syntactic sugar for:
    ```norm = lambda chan: normalize_channel(channel=chan, **kwargs)
    return apply_channelwise(image_stack, norm)
    ```
    """
    # norm = lambda chan: normalize_channel(channel=chan, **kwargs)
    return apply_channelwise(image_stack, norm)

def zscale(channel: np.ndarray, consider_mode: int, scale_q: int,
           n_bins: int = 100) -> np.ndarray:
    """Z-Scale an image using histogram mode and quantile

    Parameter
    ---------
    channel: np.ndarray
        raw data to preprocess in channel
    consider_mode: int
        Quantile of histogram in which the mode is looked for
        Should be in (1, 100)
    scale_q: int
        Quantile to scale to _after_ centering
    n_bins: int
        Number of bins to use for histogram (default=100).

    Returns
    -------
    scaled: np.array
        Copy of scaled input with same shape
    """
    pixels = channel.ravel().copy()

    # center
    firstq_m = pixels <= np.quantile(pixels, consider_mode)
    hist, bins = np.histogram(pixels[firstq_m], bins=n_bins)
    pixels = pixels - mode_value(hist, bins)

    # throw away negs and scale
    pixels[pixels < 0] = 0
    pixels = pixels / np.quantile(pixels, scale_q)
    pixels[pixels > 1] = 1

    return pixels.reshape(channel.shape)


def pnormalize_channel(channel: np.ndarray, consider_mode: float,
                       cutoff: float = 0.2, n_bins: int = 100,
                       keep_histograms: bool = False) -> np.ndarray:
    """
    Normalize images using Gaussian Mixture

    Tries to detect foreground and background modes in `channel`
    histogram. Likelyhood of foreground is then used as squeezing
    function for pixel values.

    Parameters
    ----------
    channel : np.ndarray
        Imagedate as ndarray subclass. Expects an meta attribute.
        This is always present when using channels from `ImageStack`
        or `imageio.core.Arrays`. Histogram data will be stored in
        the meta atribute dict of channel dict
    consider_mode : float
        Quantile in which the mode is looked for. Must in (0, 1)
    n_bins : int
        Number of bins for histogram calculation
    cutoff : float
        Defines which values ar considered for fitting. All values
        smaller than maximum_count * cutoff are ignored in fitting. This is
        done to remove leading tails in the histogram
        (default=0.2)
    keep_histograms: bool
        If False (default) drop all calculated histograms and quantities.
        If True, they are attached to the channel metadata

    Returns
    -------
    np.ndarray:
        Copy of the normalized pixel values in shape of `channel`

    Note
    ----
    Makes some strong assumptions about the data. Especially that
    Most of the pixels in `channel` are background pixels. Thus that
    the probability density of the background is larger than the foreground
    probability density. Also that the largest count/mode is the mean of the
    backround density.

    Values larger then 2 times the forground std-dev are clipped!

    tl;dr: Expects very well behaved, no-funny stuff data. Please make sure
    these assumptions are met.
    """

    # Histogram in quantile considered to have the mode (most often background)
    # mask saturated pixels to prevent overflow
    pixels = channel.ravel()
    try:
        unsaturated = pixels < np.iinfo(channel.dtype).max
    except ValueError:
        unsaturated = pixels < np.finfo(channel.dtype).max
    pixels[unsaturated] = np.log2(pixels[unsaturated] + 1)
    firstq_m = pixels <= np.quantile(pixels, consider_mode)
    hist, bins = np.histogram(pixels[firstq_m], bins=n_bins)

    # center pixels by mode
    pixels = pixels - mode_value(hist, bins)

    # calculate normalized histogram of the now centerd pixel values
    # cutoff index as percentage from hist.max()
    # Could be as well std or something, but we still have the
    # foreground tail, and this is done to aid in fitting the gaussians
    # a even better alternative would be to iterate throug all bins,
    # to the left and keep only bins that smaller than the last one.
    # to smoothen out the curve. then interpolate between all existing
    # values.
    hist, bins = np.histogram(pixels, bins=n_bins, density=True)
    cutoff_idx = np.argmax(hist >= hist.max() * cutoff) + 1

    # consider only the valid points, calculate the bin center as
    # support for the densities, use BayesioanGaussianMixture to fit
    # the histogram. simple GaussianMixture might work as well. But
    # as we have a strong assumption on where the first Mode should live
    # we have a pretty good prior. Simple GMM often suffers mode collaps
    hist = hist[cutoff_idx:]
    sup = ((bins[1:] + bins[:-1]) * .5)[cutoff_idx:]
    bgm = BayesianGaussianMixture(
        n_components=2, covariance_type='diag', tol=0.001, reg_covar=1e-06,
        max_iter=100, n_init=10, init_params='kmeans',
        weight_concentration_prior_type='dirichlet_process',
        weight_concentration_prior=1, mean_precision_prior=1, mean_prior=[0],
        degrees_of_freedom_prior=None, covariance_prior=[0.1],
        random_state=None, warm_start=False, verbose=0, verbose_interval=10)
    bgm.fit(sup.reshape(-1, 1), hist.reshape(-1, 1))

    # extract the parameter from fit
    centers = bgm.means_.squeeze()
    # should not be nessescary, but it happend. Maybe just a remain
    # from using GMM, where label-swpaps might happen but better keep it as
    # sanity and safety check
    sort_idx = np.argsort(centers)
    std = np.sqrt(bgm.covariances_.squeeze())
    mixin = bgm.weights_.squeeze()
    centers = centers[sort_idx]
    std = std[sort_idx]
    #TODO super flunky. Something is off...
    mixin = 1 - mixin[sort_idx]

    # center pixels by fitted mode center
    pixels = pixels - centers[0]
    centers = centers - centers[0]

    # scale to 2 time std of second (foreground) mode
    scale = centers[1] + (2 * std[1])
    std = std / scale
    centers = centers / scale
    pixels = pixels / scale
    sup /= scale

    # calculate fg/bg probabilities for pixel values
    modes = [
        stats.norm.pdf(pixels, mui, stdi) for mui, stdi in zip(centers, std)]

    f_norm = modes[1] * mixin[1] / np.sum(
        [mo * mi for mo, mi in zip(modes, mixin)], axis=0)
    # squeeze and clip pixels
    f_norm = np.clip(pixels * f_norm, 0, 1)

    # make output channel
    ret = f_norm.reshape(channel.shape)

    # calculate pdf and squeeze function and store it as meta data attribute
    # in an structured array
    bgfg_density = [
        stats.norm.pdf(sup, mui, stdi) for mui, stdi in zip(centers, std)]

    squish_func = bgfg_density[1] * mixin[1] / np.sum(
        [mo * mi for mo, mi in zip(bgfg_density, mixin)], axis=0)
    
    if keep_histograms:
        data = [sup.squeeze(), hist.squeeze(), squish_func.squeeze(),
                bgfg_density[0], bgfg_density[1]]
        rec_dt = [('sup', 'f4'), ('hist', 'f4'), ('squish', 'f4'), ('bg', 'f4'),
                  ('fg', 'f4')]
        meta_norm = np.rec.fromarrays(data, dtype=rec_dt)
        ret.meta['histnorm'] = meta_norm

    return ret
