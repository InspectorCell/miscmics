from pathlib import Path
from typing import Tuple

import numpy as np
# from skimage import (
#     color, feature, filters, measure, morphology, segmentation, util
# )
# from skimage.filters.rank import entropy
from skimage.feature import peak_local_max, canny
from skimage.filters import threshold_multiotsu, gaussian
from skimage.morphology import disk, closing, binary_dilation
from scipy import ndimage as ndi


def quantil_normalize(dapi_image, q_center, q_scale):
    """ Normalize and clip dapi images on quantil
    """
    dapi_im = dapi_image - np.quantile(dapi_image.ravel(), q_center)
    return np.clip(dapi_im / np.quantile(dapi_im.ravel(), q_scale), 0, 1)

def substract_gaussian(dapi_im, sigma, diff_factor=0.3, **kwargs):
    """ Create diff image from original and Gaussian blurr image
    """
    dapi_g = gaussian(dapi_im, sigma=sigma, **kwargs)
    diff_im = np.clip(dapi_im - (dapi_g * diff_factor), 0, 1)
    return diff_im, dapi_g

def substract_canny(dapi_im, sigma, diff_factor=1, dilate=True, **kwargs):
    """ Create diff image from original and Gaussian blurr image
    """
    dapi_e = canny(dapi_im, sigma=sigma, **kwargs)
    
    if dilate:
        dapi_e = binary_dilation(dapi_e)

    diff_im = np.clip(dapi_im - (dapi_e * diff_factor), 0, 1)
    return diff_im, dapi_e

def disttransform_otsu(dapi_img, classes=2, closedisc=2):
    thresholds = threshold_multiotsu(dapi_img, classes=classes)
    regions = np.digitize(dapi_img, bins=thresholds)
    cell_mask = regions > 0
    closed_cell_mask = closing(cell_mask, disk(closedisc))
    return ndi.distance_transform_edt(closed_cell_mask)

def get_dapiregions(dapi_image: np.ndarray,
                    normalize: Tuple[float] = (0.03, 0.97),
                    sigma: float = 3,
                    diff_factor: float = 0.75,
                    min_dist: int = 7) -> np.ndarray:
    """
    Takes an 1D Dapi image in an ndarray and returns an array of points

    DAPI image is centered by quantil values of dapi image. Values outside the
    interval [0, 1] are clipped afterwards.

    Paramter
    --------
    dapi_image: np.ndarray
        Dapi image date of shape (N, M)
    normalize: Tuple[float]
        Defines quantils used for normalization (q_center, q_scale). Both values
        must be floats from in the interval (0, 1)
    sigma: float
        Size of kernel for Gaussian filtering
    diff_factor: float
        Factor on blurr image substracted from original image
    min_dist: int
        Minimum distance between two dapi areas, used in local max filtering

    Returns
    -------
    points: np.ndarray
        Array of shape (K, 2). For each DAPI found, there is a point
    """

    # center, scaling, clipping
    q_center, q_scale = normalize
    dapi_im = quantil_normalize(dapi_image, q_center, q_scale)

    # gaussian filtering
    diff_im, _ = substract_gaussian(dapi_im, sigma, diff_factor=diff_factor)

    # dist transfrom of thresholded image
    closed_cell_dists = disttransform_otsu(diff_im)

    # watershed segmentation
    local_max_coords = peak_local_max(closed_cell_dists, min_distance=min_dist)
    return local_max_coords
