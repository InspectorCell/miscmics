""" Select images from image field data
"""

from ...multiplex.multiplex import ImageStack
from ...multiplex.imagedir import ProcessedReader
from .histpick import select_by_hist


def extract_channels(image_dirs, out_dir, use_marker,
                     pattern='^(?P<name>(.*)(?P<ext>.tiff?)$'):
    """ Extract channels by histogram in each input channel

    image_dirs: Iterable[str]
    out_dir: Union[str, Path]
    valid_marker: Iterable[str]
        Only Consider these markers
    pattern: str
        Regex pattern with named groups, where the group names are meta
        data values and the group names are the meta data keys

    returns
    -------
    mapping: Dict
        In- and output mapping
    """
    dir_mapping = {}
    ims = []
    for i, a_dir in enumerate(image_dirs):
        ims = ImageStack(ProcessedReader(pattern=pattern))
        ims.read_dir(a_dir)
        dir_mapping[f'set{i}'] = {'path': str(a_dir)}
        dat = ims.find_multiple([{'marker': mname} for mname in use_marker])
        ims = ImageStack()
        for chans in dat:
            for ch in chans:
                ims.add_image(image_data=ch, meta_data=ch.meta)
        ims = select_by_hist(ims, groupkey='marker')
        if out_dir is not None:
            to_dir = out_dir / f'set{i}'
            to_dir.mkdir(parents=True)
            if to_dir.exists():
                print('Skipping exististing directory {to_dir}')
                continue
            ims.write_dir(to_dir)
        else:
            dir_mapping[f'set{i}']['ims'] = ims

    return dir_mapping
