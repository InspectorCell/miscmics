Overview
========
Python toolbox of miscellaneous functions/classes/procedures for processing multiplexed images and objects in theses images

Install
=======
With pip and git (needs pip and git)
```bash
# install into current Python environment with pip
pip install git+https://gitlab.com/rikisa/miscmics.git@main
```

With conda-build (needs miniconda/anaconda including conda-build and git)
Make sure to build in the base environment, see https://github.com/conda/conda/issues/7758
```bash
# get the tip
git clone --depth=1 --single-branch --branch=main https://gitlab.com/rikisa/miscmics.git
# meta.yaml is there 
cd miscmics/conda
# build the package locally. No versions are specified...
conda build -c conda-forge .
# install the local package. Switch to your target conda environ
# conda activate my_target_environment_where_i_want_to_install_miscmics
conda install --use-local miscmics -c conda-forge
```

From source with git
```bash
git clone --depth=1 --single-branch --branch=main https://gitlab.com/rikisa/miscmics.git
cd miscmics
python setup.py install
```
