API Doc
=======

Entities
--------
.. automodule:: miscmics.entities.entity
   :members:

.. automodule:: miscmics.entities.factory
   :members:

.. automodule:: miscmics.entities.imageentity
   :members:
   :exclude-members: drawContours

.. automodule:: miscmics.entities.jsonfile
   :members:

.. automodule:: miscmics.entities.ledger
   :members:

Multiplex
---------
.. automodule:: miscmics.multiplex.multiplex
   :members:

Processing
----------
..
  .. automodule:: miscmics.processing.entities.manipulate
     :members:
  .. automodule:: miscmics.processing.entities.extract
     :members:
  .. automodule:: miscmics.processing.entities.tabels
     :members:
  .. automodule:: miscmics.processing.entities.entitynormalization
     :members:
  .. automodule:: miscmics.processing.entities.entitynormalization
     :members:

.. automodule:: miscmics.processing.entities
   :members:

Misc
----

.. automodule:: miscmics.tecanparser
   :members:

.. automodule:: miscmics.util
   :members:
