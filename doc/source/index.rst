.. fs2ometiff documentation master file, created by
   sphinx-quickstart on Thu Nov 15 10:14:21 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MiscMics
========

Reference and some documentation version |release|

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   apidoc/classes
   CHANGELOG.rst

.. .. include:: apidoc/classes

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
