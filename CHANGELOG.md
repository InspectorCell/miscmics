Changelog
=========

Version 0.47
------------
Overview
^^^^^^^^
Small quality of life utilities

Features
^^^^^^^^
- Added ImageStack.update to update channel data easily inplace
- Readded miscmics.processing.entities.extract.create_from_pixmap, to generate a
  EntityLedger with ImageEntities from a pixmap
- Robust normalization for scalar keys of Entities

Version 0.46
------------
Overview
^^^^^^^^
Update docs and added neighborhood and spatial/nice indexing using scipy
Voronois and som QoL syntactic sugar

Features
^^^^^^^^
- Added Entity.center, geometric contour center, set upon contout update
- Finding ImageEntity neighbours and annotate them via references
- ImageStack.copy and ImageStack.astype

Bugfix
^^^^^^
- pnormalize_channel() will not overflow saturated integer pixel anymore

Version 0.45
------------
Features
^^^^^^^^
- Added ImageStack.pop() that works like ImageStack.find_multiple() but removes
  found images in-place 
- Added ImageStack.find_one() as syntastic sugar
- Added new get_center util function
- Added mapping function for EntityLedger

Depreciations
^^^^^^^^^^^^^
- Removed read_h5() from miscmics.multiplex.hdf5
- Removed ImageStack.clip()
- Removed miscmics.processing.packstacks <- to domain specific

Version 0.44
------------
Overview
^^^^^^^^
Enforce providing meta data. Contract between Reader/Writer that there is a minumum
of metadata provided.
Each ImageStack MUST have metadata 'uri' metadata. Each channel MUST have 'name'
Names must be unique within their scopes. Unique name for ImageStacks if stored
in HDF5, unique channel names when added to image stack

Features
^^^^^^^^
- Handling of invalid contours and historical entities separatly
- Util function for offsetting ImageEntity contours
- ImageStack creates generic uri only if not set and some data is added/provided at some point
- Images added to ImageStack must have at least have a 'name' metadata key
- Only greyscale images can be added -> channel shape is enforced!
- HDF5 Reader/Writer
- Added channel normalization based on foreground/background detection

Version 0.43
------------
Features
^^^^^^^^
- Crop out segments from ImageStack to specific shape

Version 0.42
------------
Overview
^^^^^^^^
Helper functions for encode ImageStacks and processing functions

Features
^^^^^^^^
- Encoder that encodes the Multiplexed image class (using tensorflow)
- General processing tools (e.g. scoring channels by histogram range)

Version 0.40
------------
Overview
^^^^^^^^
Refactoring, some bugfixes

Version 0.30
------------
Overview
^^^^^^^^
Mayor rewrite and factoring to achieve higher modularity, disentangel responsibilities
and dependencies. Renaming of the whole repo.

Features
^^^^^^^^
- Generic parser for Images
- Specific parser for TecanImages
- Generic metadata model
- Generic Multiplexed image class

Version 0.20
------------
Overview
^^^^^^^^
Rewrite to faciliate the imageio package. Just add format for mics images with a
generic reader. Breaks compability to any version < 0.20

Other
^^^^^^^^
- Use versioneer for easier versioning
- Use imageio library and just implement Format, Reader, and Writer for MICS
- Container and processing into multiplexed and util

Version 0.16
------------
Bugfix
^^^^^^^^
- Fast setting of new objects now sets the object id in all cases

Version 0.15
------------
Features
^^^^^^^^
- ImgObj have scalar values now

Version 0.14post1
-----------------
Changes
^^^^^^^^
- Obj must be smaller than 200x200 px

Bugfix
^^^^^^^^
- Correct calculation of boundingboxes
- Objets are now actually generated in threads

Version 0.14
------------
Features
^^^^^^^^
- Increased objectmap generation speed (obj must be smaller than 400x400 px now!)
- Broke down obj creation into smaller functions suitabel for multiprocessing

Version 0.13post1
-----------------
Features
^^^^^^^^
- ObjGen logs now
- Reuses ids of deleted objects
- Sanity check for adding new objects. Will raise RuntimeError if failed

Bugfix
^^^^^^^
- After deleting an object from the ObjGen the map is updated

Version 0.13
------------
Features
^^^^^^^^
- All RawImage children now keep track on where pixeldata orginated from
- Imagestack now allows to get the original source for an specifc marker 
  via get_imagedata_origin
- If ImageStack now skipps an image it will be logged as a debug information
  and not any longer as warning
- ObjGen has no add and delete interface for objects including updating of the
  objid map 

Version 0.12
------------
Features
^^^^^^^^
- ImageStack now takes an optional path to a tiny db, which is handed to the
  filename parser
- Adding single images to ImageStack
- Transparency added. RGB(0, 0, 0) will always have the alpha value 0

Version 0.11
------------
Features
^^^^^^^^
- LUT.to_rgba() now takes as scaling parameter, wich scales the input to spread
  the input array values over the whole spectrum
- Tags are now cached. Tag color is persisten for up to 655 individual tags

Bugfix
^^^^^^^
- Overlay maps using the lut are now normalized to use the whole spectrum

Version 0.10
------------
Bugfix
^^^^^^^
- Mapgen raises ValueError now, if objid_map(!) not set
- TiffImage can now handle pathlib.Path as well

Version 0.9
-----------
Features
^^^^^^^^
- ImageStack now accept the logger_domain parameter upon instanciation

Version 0.8
-----------
Features
^^^^^^^^
- First useable version
