"""Test helper utils and fucntions"""

# extern
import pytest
import numpy as np

# testees
from miscmics.entities import ImageEntity, EntityFactory
from miscmics.processing.entities import (get_sliced_mask, mask_to_contour,
    offset_ledger, get_mass_center)
from miscmics.processing.entities.entitynormalization import normalize_scalarkeys


data_test_get_center = [
    ([[[0, 0], [1, 2], [1, 0]]], [2/3, 2/3]),
    ([[[0, 0], [2, 0], [2, 2], [0, 2]],
      [[0, 2], [0, 4], [2, 4], [2, 2]]], [1, 2]),
    ([[[-0.46395214,  0.1139687 ],
       [ 1.28522212,  0.6691206 ],
       [-0.27906711, -0.56066342],
       [ 1.1003371 ,  1.34375272],
       [ 0.03574322,  0.16531621],
       [ 0.78552676,  0.61777309],
       [-0.26685415, -0.09705303]],
      [[ 1.08812414,  0.88014233],
       [ 0.15826772, -0.47017734],
       [ 0.66300226,  1.25326664],
       [ 0.21322396,  0.16481525],
       [ 0.60804602,  0.61827405],
       [ 0.10247304,  0.14059606],
       [ 0.71879694,  0.64249325]]],
      [0.41063499, 0.39154465])
]
@pytest.mark.parametrize('contour, center', data_test_get_center)
def test_get_center(contour, center):
    ent = ImageEntity()
    ent.update_contour(contour)
    assert np.allclose(get_mass_center(ent).squeeze(), center)


def test_get_masks_grey():
    mapping = np.zeros((300, 500), np.uint16)

    cur_val = 0
    values = []
    for r in (10, 15, 100, 250):
        for c in (5, 10, 230, 450):
            cur_val += 1
            values.append(cur_val)
            # fill with current value and make some edgeds
            mapping[r:r + 10, c:c + 8] = cur_val
            mapping[r + 5, c + 3] = 0
            mapping[r, c] = 0

    # a reference mask
    for cur_val in values:
        value_slice, mask = get_sliced_mask(mapping, cur_val)

        # assert everything in the mask is found
        # import IPython as ip
        # ip.embed()
        arr_slice = mapping[value_slice]
        assert np.all(arr_slice[mask] == cur_val)
        assert np.all(arr_slice[~mask] == 0)

        # assert nothin outside of masked slice
        mapping[value_slice][mask] = 0
        assert np.sum(mapping == cur_val) == 0

def test_to_contour_invalid():
    mask = np.ones((5, 5), bool)
    mask_slice = np.s_[0:4, 0:4]

    with pytest.raises(ValueError):
        _ = mask_to_contour(mask_slice, mask)

def test_from_pixmap():
    """Test moving of entities inplace
    """

    # pixmap = np.zeros((20, 20), np.uint16)
    oid_map = np.array([
        [1, 1, 1, 0, 0, 0],
        [1, 1, 1, 0, 0, 0],
        [1, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 2, 2],
        [0, 0, 0, 0, 2, 2]])

    fac = EntityFactory()

    contours = []
    for oid in (1, 2):
        cnt = mask_to_contour(*get_sliced_mask(oid_map, oid))
        ent = fac.create_entity(cls=ImageEntity)
        ent.update_contour(cnt)
        ent.generic['oid'] = oid
    
    canvas = np.zeros_like(oid_map)
    for ent in fac.ledger.entities.values():
        canvas[ent.slc][ent.mask] = ent.generic['oid']

    assert np.all(canvas == oid_map)

def test_offset_ledger():
    fac = EntityFactory()
    fac.create_entity(eid=5, contour=[[[1, 2], [3, 4]]], cls=ImageEntity)
    offset = (1, 2)

    offset_ledger(fac.ledger, offset)
    
    ent = fac.ledger.entities.get(5)
    
    ought = np.array([[0, 0], [2, 2]])
    assert np.all(ent.contour[0] == ought)

@pytest.mark.parametrize('res', ('mean', 'err', 'replace'))
def test_norm_scalarkey_no_conflict(res):
    """ Test normalization of scalar values wo conflict
    """

    fac = EntityFactory()
    ent = fac.create_entity()
    
    ent.scalars['A'] = 1
    ent.scalars['a'] = 1
    ent.scalars['B'] = -1
    ent.scalars['c'] = 100

    normalize_scalarkeys(ent, mapping=dict(A='a'), resolve=res)

    assert ent.scalars['a'] == 1
    assert 'A' not in ent.scalars

    assert ent.scalars['B'] == -1
    assert ent.scalars['c'] == 100

@pytest.mark.parametrize('res', ('mean', 'err', 'replace'))
def test_norm_scalarkey_conflict(res):
    """ Test normalization of scalar values wo conflict
    """

    fac = EntityFactory()
    ent = fac.create_entity()
    
    ent.scalars['A'] = 3
    ent.scalars['a'] = 2
    ent.scalars['B'] = -1
    ent.scalars['c'] = 100

    # expect error, testcase done
    if res == 'err':
        with pytest.raises(ValueError):
            normalize_scalarkeys(ent, mapping=dict(a='A'), resolve=res)
        assert 'a' in ent.scalars
        assert 'A' in ent.scalars
        return

    normalize_scalarkeys(ent, mapping=dict(a='A'), resolve=res)

    # for all other cases must be true
    assert 'a' not in ent.scalars

    if res == 'mean':
        assert ent.scalars['A'] == 2.5
    else:
        assert ent.scalars['A'] == 3

    # should alwys be untouched
    assert ent.scalars['B'] == -1
    assert ent.scalars['c'] == 100
