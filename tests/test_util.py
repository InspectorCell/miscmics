from miscmics.util import StrEx


def test_strre():
    s = 'abc|123|_|an_under'
    sre = StrEx(s)

    # for noteq in ('abs', 'a', 1, 1.5e-4, 123):
    #     assert noteq != sre
    #     assert sre != noteq
    #     assert not noteq == sre
    #     assert not sre == noteq

    for iseq in ('abc', '123', '_', 'an_under'):
        assert iseq == sre
        assert sre == iseq
        assert not iseq != sre
        assert not sre != iseq

    assert s in str(sre)
    assert sre != s

