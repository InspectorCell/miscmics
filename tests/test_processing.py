"""test the extraction function

overall an integration test + test for extract function
"""
import pytest
import numpy as np

from miscmics.multiplex import ImageStack
from miscmics.entities import EntityFactory, ImageEntity
from miscmics.processing.entities import (
    extract_features, mask_to_contour, get_sliced_mask)

from miscmics.processing import is_out_of_bound

from miscmics.processing.images.channelnormalization import pnormalize_channel


def get_imagedata():
    """create mock data to be used in ImageStack / EntityCreation

    create one channel for each entity
    """

    slices = (np.s_[5:10, 3:5],
              np.s_[70:75, 85:95],
              np.s_[30:40, 50:60],
              np.s_[50:60, 90:100],)

    n = len(slices)
    mask = np.zeros((80, 100), int)
    channels = np.zeros((n, 80, 100, 1))

    feat_values = np.random.random(size=n)
    object_ids = np.arange(1, n+1)

    for i, (slc, val, oid) in enumerate(
            zip(slices, feat_values, object_ids)):
        channels[i][slc] = val
        mask[slc] = oid

    return mask, channels, feat_values, object_ids


def get_data():
    """Get EntityFactory and ImageStack for testing
    """
    mask, channels, feat_values, object_ids = get_imagedata()

    # setup imagestack
    ims = ImageStack()
    for i, chan in enumerate(channels):
        
        ims.add_image(chan, meta_data={'idx': i, 'name': i})

    # setup entities
    fac = EntityFactory()
    for oid in object_ids:
        ent = fac.create_entity(cls=ImageEntity)
        ent.generic['oid'] = oid
        cnt = mask_to_contour(*get_sliced_mask(mask, oid))
        ent.update_contour(cnt)

    # entity oid 60 is out of bound
    oob, = [ent for ent in fac.ledger.entities.values() \
            if ent.generic['oid'] == 4]

    oob.tags.add('OUGHT_OOB')

    new_cont = []
    for shp in oob.contour:
        new_shape = []
        for ptx, pty in shp:
            new_shape.append((ptx + 10, pty))
        new_cont.append(new_shape)
        oob.update_contour(new_cont)

    return channels, ims, fac


def test_extract_features_scalars_keys():
    imgarr, imstack, factory = get_data()
    
    oob_tag = 'IS_OOB'
    channel_key = 'idx'
    feat_funcs = dict(
        sum=lambda x, *args: np.sum(x),
        mean=lambda x, *args: np.mean(x),
    )
    
    with pytest.warns(UserWarning) as wrn:
        extract_features(
            factory.ledger,
            imstack,
            oob_tag=oob_tag,
            key=channel_key,
            feature_functions=feat_funcs)
    assert 'out of bound' in str(wrn.pop().message)
    
    ought_keys = set()
    for fn in feat_funcs.keys():
        for i in range(len(imstack.data)):
            ought_keys.add(f'{channel_key}_{i}_{fn}')
    
    for ent in factory.ledger.entities.values():
        all_ent_keys = set(ent.scalars.keys())
        if len(all_ent_keys):
            assert all_ent_keys == ought_keys
        else:
            assert oob_tag in ent.tags


def test_extract_features_ignore_oob():
    imgarr, imstack, factory = get_data()

    with pytest.warns(UserWarning) as wrn:
        extract_features(factory.ledger, imstack, 'idx')
    assert 'out of bound' in str(wrn.pop().message)

    for ent in factory.ledger.entities.values():
        oid = ent.generic['oid']
        idx = oid - 1
        
        ori_chan = imgarr[idx]
        ims_chan, = imstack.find({'idx': idx})

        # check for promis that there is no alteration
        # when adding to image stack
        assert np.allclose(ori_chan.squeeze(), ims_chan)

        # check if out of bounds entity is tagged
        if 'OUGHT_OOB' in ent.tags:
            assert 'out-of-bounds' in ent.tags
            continue
        
        nz_mask = ims_chan != 0

        for i in range(3):
            if i != idx:
                assert ent.scalars[f'idx_{i}_sum'] == 0
                assert ent.scalars[f'idx_{i}_mean'] == 0
            else:
                assert ent.scalars[f'idx_{i}_sum'] == ims_chan[nz_mask].sum()
                assert ent.scalars[f'idx_{i}_mean'] == ims_chan[nz_mask].mean()

def test_extract_features_raise_oob():

    imgarr, imstack, factory = get_data()

    with pytest.raises(IndexError):
        extract_features(factory.ledger, imstack, 'idx', 'raise')

def test_extract_features_keyerror():

    imgarr, imstack, factory = get_data()

    with pytest.raises(KeyError):
        extract_features(factory.ledger, imstack, 'xyz', 'ignore')


data_test_oob = [
  (np.s_[-3:10, 50:60], (50, 45), True),
  (np.s_[-3:10, 35:40], (50, 45), True),
  (np.s_[ 3:10, 50:60], (50, 45), True),
  (np.s_[ 5:13, 30:45], (50, 45), False),
  (np.s_[ 3:15, 35:40], (50, 45), False),
  (np.s_[ 3:10, 35:40], (50, 45), False),
]

@pytest.mark.parametrize('box, shape, is_valid', data_test_oob)
def test_check_oob(box, shape, is_valid):
    assert is_out_of_bound(box, shape) == is_valid


def test_pnormalize_channel():

    ims = ImageStack()
    ims.add_image(np.zeros((5, 5), dtype=np.uint8), meta_data={'name': 'dummy'})
    ims.data[0][0, 0] = np.iinfo(np.uint8).max
    ims.data[0][0, 1:] = 10
    ims.data[0][1, 2:] = 200

    pnormalize_channel(ims.data[0], consider_mode=0.5)


