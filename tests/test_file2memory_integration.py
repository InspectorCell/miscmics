from uuid import UUID
import numpy as np
from pytest import mark

from miscmics.entities.jsonfile import load
from miscmics.entities import ImageEntity
from miscmics.multiplex import ImageStack
from miscmics.multiplex.imagedir import ProcessedReader
from miscmics.processing.entities.extract import generate_segment_sliceouts


@mark.filterwarnings("ignore:Stripping historic entity de1")
def test_crop(small_stack):
    imdir, segfile = small_stack

    pat = '.*ch_(?P<name>[a-z]).tif'
    ims = ImageStack(reader=ProcessedReader(pat))
    ims.read_dir(imdir)
    
    segs = load(segfile, cls=ImageEntity)

    entcrops = list(generate_segment_sliceouts(segs.ledger, ims))

    # filter historic on load per default?
    assert len(entcrops) == 2

    # check for correctness of entity data
    ent_by_chan = {}
    for ent, crop in entcrops:
        ch_tags = [t for t in ent.tags if t.startswith('ch_')]
        assert len(ch_tags) == 1, 'only one channel tag'
        ch = ch_tags[0].split('_')[-1] # a or b
        ent_by_chan[ch] = (ent, crop)
    assert set(['a', 'b']) == set(ent_by_chan.keys()), 'test data faulty'

    
    ent0, crop0 = ent_by_chan['a']
    ent1, crop1 = ent_by_chan['b']

    assert ent0.eid == UUID(
        hex='3b6d29bf591149db9bba8ec127daf14a')
    assert ent1.eid == UUID(
        hex='b560e3fd416c4871ad488b96fb8a48a6')
    
    assert crop0.shape == (11, 11, 2)
    assert crop1.shape == (31, 26, 2)
    
    # enforce correct order
    crops = []
    for crop in (crop0, crop1):
        chan_meta = crop.meta['channels']
        names = [ch['name'] for ch in chan_meta]
        if not names == ['a', 'b']:
            assert names == ['b', 'a']
            crop = crop[:, :, ::-1]
        crops.append(crop)
    crop0, crop1 = crops

    # zero channels
    assert crop0[..., 1].mean() == 0, 'Should be black'
    assert crop1[..., 0].mean() == 0, 'Should be black'
    
    # all white
    assert crop0[..., 0].mean() == 0xffff, 'All white channel'
    # house
    house_mean = (31 * 26 - 124) * 0xffff * (31 * 26)**-1
    assert np.isclose(crop1[..., 1].mean(), house_mean), 'assert we have house'
