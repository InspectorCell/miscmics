import numpy as np

from pytest import raises, warns

from miscmics.multiplex import ImageStack
from miscmics.entities import ImageEntity, EntityFactory, EntityType


def test_instanciation():

    ie0 = ImageEntity(eid=-1)
    ie1 = ImageEntity(eid=3)

    assert ie0.eid == -1
    assert ie1.eid == 3

def test_update_contour():
    """Test if mask and slice are set correct
    """
    mask = np.zeros((10, 10), np.uint8)
    mask[0:4, 1:9] = 1
    mask[7:10, 0:10] = 1
    mask_slice = np.s_[0:10, 0:10]
    boundingbox = np.array([[0, 0], [9, 9]])

    ent = ImageEntity()
    contour = np.array([
        [[0, 7], [0, 9], [9, 9], [9, 7]],
        [[1, 0], [1, 3], [8, 3], [8, 0]]
    ])
    ent.update_contour(contour)
    assert np.array_equal(ent.mask, mask)
    assert ent.slc == mask_slice
    assert np.allclose(ent.bbox, boundingbox)

def test_update_contour2():
    """Test if slice cuts correctly from image (block shape)
    """
    image = np.ones((30, 30, 4))
    image[20:25, 10:15, :] = 0

    contour = np.array([
        [[10, 20], [14, 20], [14, 24], [10, 24]],
    ])

    ent = ImageEntity()
    ent.update_contour(contour)

    assert image[ent.slc].size == np.sum(image == 0)
    assert np.all(image[ent.slc] == 0)

def test_center_set():
    """Test if center point is set (correctly)
    """
    contours = (
        [[[1, 2], [1, 1], [1, 2], [1, 0]]], # single shape
        [[[1, 2], [1, 1]],
         [[1, 2], [1, 0]]] # same points, but split
    )
    # -> sums: (4, 5) -> centers: (1, 1.2)
    ought = np.array([1, 1.25])


    ents = (ImageEntity(), ImageEntity())
    for i, cnt in enumerate(contours):
        ents[i].update_contour(cnt)

    assert np.allclose(ents[0].center, ought)
    assert np.allclose(ents[1].center, ought)

def test_update_contour_drawing():
    """Test if slice and mask match up to source image with ObjIds
    """
    # source image with ObjIds
    ydim, xdim = (99, 200)
    src_img = np.zeros(ydim * xdim).reshape(ydim, xdim)
    dst_img = np.zeros(ydim * xdim).reshape(ydim, xdim)

    # ObjIds to use in src img
    eids = (0.5, 0.1, 0.8)
    # slices in src image corresponding to the mask
    slices = (
        # just one rectangel
        [np.s_[10:20, 2:5]],

        # multiple rectangles
        [np.s_[50:71, 20:49], np.s_[70:81, 25:50], np.s_[72:81, 24:26]],

        # must fit the image
        [np.s_[80:91, 150:161], np.s_[85:91, 160:171]],
        )

    # draw source image
    for cur_eid, cur_slices in zip(eids, slices):
        for sub_slice in cur_slices:
            src_img[sub_slice] = cur_eid

    # contours corresponding to the mask/slices
    contours = (
        # first, simple block contour
        np.array(
            [[[2, 10],
              [2, 19],
              [4, 19],
              [4, 10]]], int),

        # second, composit block contour
        np.array(
            [[[20, 50],
              [20, 70],
              [25, 70],
              [25, 72],
              [24, 72],
              [24, 80],
              [49, 80],
              [49, 70],
              [48, 70],
              [48, 50]]], int),

        # fitting contour
        np.array(
            [[[150, 80],
              [160, 80],
              [160, 85],
              [170, 85],
              [170, 90],
              [150, 90]]], int)
    )

    ents = []
    for cur_eid, cur_cont in zip(eids, contours):
        ent = ImageEntity()
        ent.update_contour(cur_cont)
        ent.scalars['src_id'] = cur_eid
        ents.append(ent)

    for cur_ent in ents:
        dst_img[cur_ent.slc][cur_ent.mask] = cur_ent.scalars['src_id']

    # idx = np.where(src_img != dst_img)
    # src_img[idx] = -1
    # import matplotlib.pyplot as plt
    # f, ax = plt.subplots(1, 2, sharex=True, sharey=True)
    # ax[0].imshow(src_img)
    # ax[1].imshow(dst_img)
    # plt.show()

    # import IPython as ip
    # ip.embed()

    assert np.all(src_img == dst_img)

def test_update_contour_iterable():
    """Test invarinace towards array, listoflists
    -> iterables
    """
    image = np.ones((30, 30, 4))
    image[10:15, 20:25, :] = 0

    contours = [
        [[[10, 20], [14, 20], [14, 24], [10, 24]],],
        [[[0, 7], [0, 9], [9, 9], [9, 7]],
         [[1, 0], [1, 3], [8, 3], [8, 0]],],
    ]

    ent0 = ImageEntity()
    ent1 = ImageEntity()
    for cnt in contours:
        ent0.update_contour(cnt)
        ent1.update_contour(np.array(cnt))

        assert ent0.slc == ent1.slc

def test_update_with_emtpy_contour():
    """Test if error is raised for invalid contours
    """
    ent = ImageEntity()

    with raises(ValueError) as err:
        ent.update_contour([])
        assert 'invalid' in str(err).lower()
        assert 'contour' in str(err).lower()

    with raises(ValueError) as err:
        ent.update_contour([[]])
        assert 'invalid' in str(err).lower()
        assert 'contour' in str(err).lower()

    with raises(ValueError) as err:
        ent.update_contour([[], []])
        assert 'invalid' in str(err).lower()
        assert 'contour' in str(err).lower()

    with raises(ValueError) as err:
        ent.update_contour([[[]], []])
        assert 'invalid' in str(err).lower()
        assert 'contour' in str(err).lower()

def test_update_historic_valid():
    """Test if error is raised for invalid contours
    """
    ent = ImageEntity()
    ent.etype = EntityType.Historic

    with warns(UserWarning) as wrn:
        ent.update_contour([[0, 0], [1, 0], [1, 1], [0, 1]])
    assert str(ent.eid) in str(wrn[-1].message)

def test_update_historic_invalid():
    """Test if error is raised for invalid contours
    """
    ent = ImageEntity()
    ent.etype = EntityType.Historic

    with warns(UserWarning) as wrn:
        ent.update_contour([[[]], []])
    assert str(ent.eid) in str(wrn[-1].message)

def test_instanciate_historic_invalid():
    """Test if error is raised for invalid contours
    """
    with warns(UserWarning) as wrn:
        ent = ImageEntity(etype=EntityType.Historic, contour=[[], []])
    assert str(ent.eid) in str(wrn[-1].message)
    assert ent.etype == EntityType.Historic
    assert ent.contour == []

def test_imageentity_hash():
    """test if two entities ar hashable and have different hash values
    """
    fac = EntityFactory()
    ent0 = fac.create_entity(cls=ImageEntity)
    ent1 = fac.create_entity(cls=ImageEntity)

    assert ent0.eid != ent1.eid
    assert hash(ent0) != hash(ent1)

    assert len({ent0, ent1}) == 2

    """Test if error is raised for invalid contours
    """
    with warns(UserWarning) as wrn:
        ent = ImageEntity(etype=EntityType.Historic, contour=[[], []])
    assert str(ent.eid) in str(wrn[-1].message)
    assert ent.etype == EntityType.Historic
    assert ent.contour == []
