"""Tests for basic classes and functions for entities
"""
from uuid import uuid4

from pytest import mark, fixture, raises
import numpy as np

from miscmics.entities import EntityFactory, Entity, EntityLedger, EntityType
from miscmics.util import SingletonMixin


@fixture
def entity_dat():

    return dict(
        eid=uuid4(),
        etype=EntityType.Undefined,
        ref=uuid4(),
        tags=set(['a', 'b', 'v']),
        scalars=dict(minus=1, answer=42),
        generic=dict(rnd=np.random.random((5, 3, 4)), anint=5),
        contour=np.random.random((2, 4, 2)))

def test_creation():
    """Test default entity creation
    """
    fac = EntityFactory()
    ent = fac.create_entity()
    assert isinstance(ent, Entity)

def test_creation_eid():
    """Test default entity creation
    """
    fac = EntityFactory()

    eids = [uuid4() for i in range(3)]
    ents = [fac.create_entity(eid) for eid in eids]

    assert eids[0] != eids[1]
    eid_cmp = (eid == ent.eid for eid, ent in zip(eids, ents))
    assert all(eid_cmp)

def test_creation_dat(entity_dat):
    """Test default entity creation
    """
    fac = EntityFactory()
    ent = fac.create_entity(**entity_dat)
    assert isinstance(ent, Entity)
    assert ent.eid == entity_dat['eid']
    assert np.allclose(ent.contour, entity_dat['contour'])
    assert not ent.contour is entity_dat['contour']

def test_ledger_singleton_mixin():
    """Test if ledger is singleton
    """
    class SingLedger(SingletonMixin, EntityLedger):
        """ Singleton Ledger
        """

    fac = EntityFactory(ledger=SingLedger.instance())
    ent = fac.create_entity()

    assert ent.eid in SingLedger.instance()

@mark.dependency()
def test_ledger():
    """Test default entity creation
    """
    fac = EntityFactory()
    ent = fac.create_entity()
    assert ent.eid in fac.ledger

@mark.dependency(depends=['test_ledger'])
def test_eid_unique():
    """Test if doublicates are not created and error is raised

    EntityLedger is needed in order to track doublicates
    """
    fac = EntityFactory()
    ent0 = fac.create_entity()
    with raises(ValueError):
        _ = fac.create_entity(ent0.eid)

    ent1 = fac.create_entity(ent0.eid, raise_eid=False)

    assert ent0 is not ent1
    assert ent1 is None


@mark.dependency(depends=['test_ledger'])
def test_from_dict():
    """Test if doublicates are not created and error is raised

    EntityLedger is needed in order to track doublicates
    """
    fac = EntityFactory()
    
    ent_dict = {
            'eid': uuid4(),
            'generic': {'a': 1},
    }
    _ = fac.create_entity(**ent_dict)
    ent = fac.ledger.entities[ent_dict['eid']]
    assert ent.generic['a'] == 1

def test_entityfactory_class():
    """Test if correct class is instanciated
    """
    class NewEnt(Entity):
        pass

    fac = EntityFactory()
    ent = fac.create_entity(cls=NewEnt)

    assert isinstance(ent, NewEnt)

    # assert sublass checking
    with raises(ValueError):
        _ = fac.create_entity(cls=object)

    # assert interface checking
    # must take at least eid
    class Faulty(Entity):
        def __init__(self):
            pass

    with raises(TypeError):
        _ = fac.create_entity(cls=Faulty)

def test_ledger_map():
    """Test entity ledger mapping
    """
    fac = EntityFactory()
    ent0 = fac.create_entity()
    ent1 = fac.create_entity()

    ent0.generic['FIELD'] = 1
    ent1.generic['FIELD'] = 2

    ledger = fac.ledger
    
    # assert mapping w/o sideffects
    func = lambda e: e.generic['FIELD'] * 0.5
    mapping = ledger.map_entities(func)
    assert set(mapping) == set([0.5, 1.0])

    # assert mapping w sideffects
    # hahahahahahahaha
    # func = lambda e: (lambda _: None)(e.generic['FIELD'] := 3) if e.generic['FIELD'] > 1 else None
    def func2(e):
        if e.generic['FIELD'] > 1:
            e.generic['FIELD'] += 1
        return None
    mapping = ledger.map_entities(func2)
    assert mapping == [None, None]
    assert ent1.generic['FIELD'] == 3  # changed inplace
    assert ent0.generic['FIELD'] == 1  # should stay

def test_entity_hash():
    """test if two entities ar hashable and have different hash values
    """
    fac = EntityFactory()
    ent0 = fac.create_entity()
    ent1 = fac.create_entity()
    
    assert ent0.eid != ent1.eid
    assert hash(ent0) != hash(ent1)

    assert len({ent0, ent1}) == 2
