""" Test the neigbourhood calculation for entities
"""
from pytest import fixture, mark

# for creating mock data
import numpy as np

from miscmics.entities import EntityFactory, ImageEntity
from miscmics.processing.entities.extract import annotate_neighbours

@fixture
def ledger():

    contours = [[[[0.,0.],[0.,1.],[1.,1.],[1.,0.]]],
                [[[2.,0.],[2.,1.],[3.,1.],[3.,0.]]],
                [[[4.,0.],[4.,1.],[5.,1.],[5.,0.]]],
                [[[6.,0.],[6.,1.],[7.,1.],[7.,0.]]],
                [[[8.,0.],[8.,1.],[9.,1.],[9.,0.]]],
                [[[0.,2.],[0.,3.],[1.,3.],[1.,2.]]],
                [[[2.,2.],[2.,3.],[3.,3.],[3.,2.]]],
                [[[4.,2.],[4.,3.],[5.,3.],[5.,2.]]],
                [[[6.,2.],[6.,3.],[7.,3.],[7.,2.]]],
                [[[8.,2.],[8.,3.],[9.,3.],[9.,2.]]],
                [[[0.,4.],[0.,5.],[1.,5.],[1.,4.]]],
                [[[2.,4.],[2.,5.],[3.,5.],[3.,4.]]],
                [[[4.,4.],[4.,5.],[5.,5.],[5.,4.]]],
                [[[6.,4.],[6.,5.],[7.,5.],[7.,4.]]],
                [[[8.,4.],[8.,5.],[9.,5.],[9.,4.]]],
                [[[0.,6.],[0.,7.],[1.,7.],[1.,6.]]],
                [[[2.,6.],[2.,7.],[3.,7.],[3.,6.]]],
                [[[4.,6.],[4.,7.],[5.,7.],[5.,6.]]],
                [[[6.,6.],[6.,7.],[7.,7.],[7.,6.]]],
                [[[8.,6.],[8.,7.],[9.,7.],[9.,6.]]],
                [[[0.,8.],[0.,9.],[1.,9.],[1.,8.]]],
                [[[2.,8.],[2.,9.],[3.,9.],[3.,8.]]],
                [[[4.,8.],[4.,9.],[5.,9.],[5.,8.]]],
                [[[6.,8.],[6.,9.],[7.,9.],[7.,8.]]],
                [[[8.,8.],[8.,9.],[9.,9.],[9.,8.]]],
                [[[0.,10.],[0.,24.],[1.,24.],[1.,10.]]],
                [[[2.,10.],[2.,24.],[3.,24.],[3.,10.]]],
                [[[4.,18.],[4.,19.],[5.,19.],[5.,18.]]],
                [[[6.,10.],[6.,24.],[7.,24.],[7.,10.]]],
                [[[8.,10.],[8.,24.],[9.,24.],[9.,10.]]],
                [[[0.,25.],[0.,26.],[9.,26.],[9.,25.]]],
                [[[4.,17.],[5.,17.]]]]

    # map contour id to entity ids
    eid_mapping = {}
    fac = EntityFactory()
    for cnt_id, contour in enumerate(contours, 1):
        ent = fac.create_entity(cls=ImageEntity,
                          contour=contour,
                          generic={
                            'contour_id': np.array([cnt_id])}
                          )
        eid_mapping[cnt_id] = ent.eid
        eid_mapping[ent.eid] = cnt_id

    return fac.ledger


@mark.parametrize('cont_id, dist, ought', [
    [ 1, 3, (2, 6, 7)], # corner case
    [13, 3, (7, 8, 9, 12, 14, 17, 18, 19)],
    [32, 3, (27, 28, 29)],
    [31, 3, ()],
    [31, 100, (26, 28, 30)], # now it has neighbours
    [13, 100, (7, 8, 9, 12, 14, 17, 18, 19)],  # same as before
    [7, 100, (1, 2, 3, 6, 8, 11, 12, 13)], # with unbounded neighbours
    [7, 2, (2, 6, 8, 12)], # only principal directions
])
def test_neigbour_calculation(ledger, cont_id, dist, ought):
    """ Test Neighbourhood Annotator, creates a new ledger
    with new entities, that have a new generics annotation
    which are correct
    """
    # set the'neighbours', independent of inplace
    # as it gets overwritten each time
    annotate_neighbours(ledger, max_dist=dist)

    ent = None
    for ent in ledger.entities.values():
        if ent.generic['contour_id'] == cont_id:
            break
    assert ent is not None, 'Test is faulty!'

    want = set(o.eid for o in ledger.entities.values() \
            if o.generic['contour_id'] in ought)
    got = set(ent.generic['neighbours'])

    # wanti = [int(e.generic['contour_id']) \
    #         for e in ledger.entities.values() if e.eid in want]
    # goti = [int(e.generic['contour_id']) \
    #         for e in ledger.entities.values() if e.eid in got]
    # wanti.sort()
    # goti.sort()
    # print(
    #     f'idx: {ent.generic["contour_id"]}\nwant: {wanti}\n' +
    #     f'got: {goti}\ndiff: {set(wanti).symmetric_difference(goti)}')

    assert want == got


def test_inplace_update():
    """ Tes tinplace and copy flag
    """
    fac = EntityFactory()

    mock_cnt = np.array([[[0, 0], [0, 1], [1, 1]]])
    ori_ent = fac.create_entity(
        cls=ImageEntity, contour=mock_cnt.tolist())

    # needed for voronoid tessalation
    for i, j in [(0, 1), (1, 0)]:
        _ = fac.create_entity(
            cls=ImageEntity, contour=(mock_cnt + [i, j]).tolist())

    new_ledger = annotate_neighbours(fac.ledger, 20, inplace=False)
    new_ent = next(iter(new_ledger.entities.values()))
    assert not new_ent is ori_ent

    same_ledger = annotate_neighbours(fac.ledger, 20, inplace=True)
    same_ent = next(iter(same_ledger.entities.values()))
    assert same_ent is ori_ent
    assert same_ledger is fac.ledger
