# -*- coding: utf-8 -*-

"""Test shared data
"""
import sys
from pathlib import Path
sys.path.append(str(Path('.').resolve()))
print(sys.path)
from pytest import fixture


@fixture
def small_stack():
    root = Path(__file__).parent / 'res'
    imdir = root / 'small_stack'
    entfile = root / 'small_stack.ent'
    return imdir, entfile
