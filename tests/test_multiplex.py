import imageio
import numpy as np
from pathlib import Path
import platform

from pytest import raises, mark

from miscmics.multiplex import ImageStack
from miscmics.multiplex.multiplex import generic_uri, apply_channelwise
from miscmics.util import StrEx


def get_rimg(n=None, c=1):
    """creat an random image
    """
    scale = np.random.uniform(-0xfff, 0xfff)
    offset = np.random.uniform(-0xff, 0xff)

    if n is None:
        n = np.random.randint(3, 5)  # images

    if c is None:
        c = np.random.randint(1, 5)  # channels

    size = tuple([
        n,
        np.random.randint(8, 12),    # ydim
        np.random.randint(10, 15),   # xdim
        c
        ])

    return np.random.uniform(size=size) * scale + offset


def test_instanciation():
    """just test init interface
    """
    ims = ImageStack(reader=None)
    assert ims is not None

    ims1 = ImageStack.fromarray([], meta={'hi': 'there'})
    assert ims1 is not None
    assert ims1.meta.get('hi') == 'there'


def test_adding_finding_images():
    """test if images are added correctly
    """
    ims = ImageStack(reader=None)

    rimg = get_rimg(n=3)
    for i, img in enumerate(rimg):
        ims.add_image(img, {'i': i, 'name': f'i{i}'})

    for i, img in enumerate(rimg):
        stored, = ims.find({'i': i})
        assert stored is not rimg[i]
        assert np.allclose(stored, rimg[i].squeeze())

@mark.dependency()
def test_finding_all():
    ims = ImageStack(reader=None)

    n = 5
    j = 3
    rimg = get_rimg(n)
    for i, img in enumerate(rimg[:j]):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': i, 'mul': -1, 'name': i})

    res = ims.find({'mul': -1}, single=False)
    assert len(res) == j

    res = ims.find({'mul': -1}, single=True)
    assert len(res) == 1

def test_finding_restring():
    """Integrationtest with StrEx
    """
    ims = ImageStack(reader=None)

    n = 5
    j = 2
    rimg = get_rimg(n)
    for i, img in enumerate(rimg[:j]):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': i, 'ia': f'id{i+2}', 'mul': -1, 'name': i})

    trgt = [f'id{i+2}' for i in range(j)]
    strex = StrEx('|'.join(trgt))
    res = ims.find({'ia': strex}, single=False)
    assert len(res) == j

    res = ims.find({'ia': strex}, single=True)
    assert len(res) == 1

@mark.dependency()
def test_find_multiple():
    ims = ImageStack(reader=None)

    n = 3
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': str(i), 'name': i})

    ret = ims.find_multiple([{'i': StrEx('0|1|2')} for _ in range(3)])
    # thre times the same bc StrEx('0|1|2') matches all
    assert len(ret) == 3
    for r in ret:
        assert len(r) == 3

@mark.dependency(depends=['test_find_multiple'])
def test_popped_return():
    ims = ImageStack(reader=None)

    n = 10
    pop = 6, 2
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': str(i), 'name': i})
    
    mq = [{'i': str(pi)} for pi in pop]
    ret = list(ims.pop(mq))
    ret.sort(key=lambda ch: ch.meta['name'])

    # assert correct metadata
    popped_names = set(ch.meta['name'] for ch in ret)
    assert popped_names == set(pop)
    
    for popped in ret:
        assert np.allclose(rimg[popped.meta['name']].squeeze(), popped.squeeze())

@mark.dependency(depends=['test_find_multiple'])
def test_popped_remove():
    ims = ImageStack(reader=None)

    n = 10
    pop = 3, 5
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': str(i), 'name': i})
    
    mq = [{'i': str(pi)} for pi in pop]
    _ = ims.pop(mq)

    for dat in ims.data:
        assert dat.meta['name'] not in pop
        for pi in pop:
            assert not np.allclose(dat.squeeze(), rimg[pi].squeeze())

def test_get_ndimg_unaligned():
    ims = ImageStack(reader=None)

    n = 3
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg[:-1]):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': str(i), 'name': i})

    # faulty one
    ndimg = imageio.core.Array(rimg[-1, :-3, :-2])
    ims.add_image(ndimg, {'i': n-1, 'name': i+1})

    with raises(ValueError):
        _ = ims.get_ndimage()

def test_get_stack_ambigious():
    ims = ImageStack(reader=None)

    n = 3
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': str(i), 'sa': 'me', 'name': i})

    with raises(ValueError):
        _ = ims.get_stack([{'sa': 'me'} for i in range(n)])

def test_get_stack_order():
    ims = ImageStack(reader=None)

    n = 3
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': i, 'sa': 'me', 'name': i})

    i = 0
    j = 1
    
    # validate test assumptions
    assert np.allclose(rimg[i].squeeze(), ims.data[i])
    assert not np.allclose(rimg[i].squeeze(), ims.data[j])

    # the test of order itself -> order now j, i
    sub_stack = ims.get_stack([{'i': j}, {'i': i}])
    assert not np.allclose(rimg[i].squeeze(), sub_stack.data[i])
    assert np.allclose(rimg[i].squeeze(), sub_stack.data[j])

def test_find_identity():
    ims = ImageStack(reader=None)

    n = 3
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': i, 'sa': 'me', 'name': i})

    res, = ims.find({'i': 0})
    assert res is ims.data[0]

def test_get_stack_copy_ref():
    ims = ImageStack(reader=None)

    n = 3
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': i, 'sa': 'me', 'name': i})
    
    m = 2
    query = [{'i': i} for i in range(m)]
    
    # check identities
    ref = ims.get_stack(query, False)
    cpy = ims.get_stack(query, True)
    assert ref.data[0] is ims.data[0]
    assert cpy.data[0] is not ims.data[0]

def _check_same_data(ims0, ims1):
    assert len(ims0.data) == len(ims1.data)
    for img0, img1 in zip(ims0.data, ims1.data):
        assert np.allclose(img0, img1)
        sub_m = set(img0.meta.items())
        ims_m = set(img1.meta.items())
        assert sub_m == ims_m

def test_get_stack_correct():
    ims = ImageStack(reader=None)

    n = 3
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': i, 'sa': 'me', 'name': i})
    
    m = 2
    query = [{'i': i} for i in range(m)]
    
    ref = ims.get_stack(query, False)
    cpy = ims.get_stack(query, True)
    
    assert ref.meta is not ims.meta
    assert cpy.meta is not ims.meta
    ims_items = set(ims.meta.items())
    cpy_items = set(cpy.meta.items())
    ref_items = set(ref.meta.items())
    assert ims_items == ref_items
    assert ims_items == cpy_items

    ims.data = ims.data[:-1]
    _check_same_data(ims, cpy)
    _check_same_data(ims, ref)

def test_default_uri_ims():
    ims = ImageStack(reader=None)
    ims.add_image(np.zeros((3, 4, 1)), meta_data=dict(name='bar'))
    _, exe, cwd, script = ims.meta['uri'].split('||')
    assert exe.startswith('EXE')
    assert cwd.startswith('CWD')
    assert script.startswith('PYS')
    
    # in a testsetting it should always work
    for part in (exe, cwd, script):
        assert Path(part[3:]).exists()

def test_default_uri_func():
    uri_wo = generic_uri()
    _, exe, cwd, script = uri_wo.split('||')
    assert exe.startswith('EXE')
    assert cwd.startswith('CWD')
    assert script.startswith('PYS')
    
    # in a testsetting it should always work
    for part in (exe, cwd, script):
        assert Path(part[3:]).exists()

def test_default_uri_func_path():
    some_path = '/some/path'
    uri_wo = generic_uri(some_path)
    _, exe, cwd, script, apath = uri_wo.split('||')
    assert exe.startswith('EXE')
    assert cwd.startswith('CWD')
    assert script.startswith('PYS')
    assert apath.startswith('RES')
    assert apath.endswith(some_path)
    
    # in a testsetting it should always work
    for part in (exe, cwd, script):
        assert Path(part[3:]).exists()


@mark.dependency(depends=['test_finding_all'])
def test_get_ndimg_order_shape_meta():
    ims = ImageStack(reader=None, meta={'ibims': 'metadata'})

    n = 3
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': str(i), 'sa': 'me', 'name': i})

    ndimg = ims.get_ndimage()
    
    # check if metadata is copied from ims
    assert platform.node() in ndimg.meta.get('uri', '')
    assert ndimg.meta.get('ibims') == 'metadata'
    assert len(ndimg.meta.get('channels', [])) == n
    
    for i in range(n):
        mqi = ndimg.meta['channels'][i]
        chi, = [ret.squeeze() for ret in ims.find(mqi)]
        ori = rimg[i].squeeze()
        # If this fail dependency shoudl fail too
        assert np.allclose(chi, ori)

        # compare that querie is in correct place in ndimg
        assert np.allclose(chi, ndimg[..., i])
    
    assert not np.allclose(rimg[0].squeeze(), ndimg[..., -1])

def test_raises_invalid_image():

    rimg = get_rimg(n=3)

    ims = ImageStack(meta=dict(uri='testing'))

    with raises(ValueError):
        ims.add_image(rimg, {'i': 1, 'name': 'Otto'})

def test_merging_meta():
    ims = ImageStack(meta=dict(uri='testing'))
    
    rimg, = get_rimg(n=1)
    rimg = imageio.core.Array(rimg)
    rimg.meta['name'] = 'Foo'
    ims.add_image(rimg, {'i': 1})

    assert ims.data[0].meta.get('name') == 'Foo'
    assert ims.data[0].meta.get('i') == 1

def test_raises_conflicting_meta():
    ims = ImageStack(meta=dict(uri='testing'))
    
    rimg, = get_rimg(n=1)
    rimg = imageio.core.Array(rimg)
    rimg.meta['name'] = 'Foo'

    with raises(ValueError):
        ims.add_image(rimg, {'i': 1, 'name': 'bar'})

def test_unique_name():
    ims = ImageStack(meta=dict(uri='testing'))
    
    rimg, = get_rimg(n=1)
    rimg = imageio.core.Array(rimg)
    rimg.meta['name'] = 'Foo'

    ims.add_image(rimg)

    with raises(ValueError) as err:
        ims.add_image(rimg)
    err.match('.*Foo.*already.*exists')

def test_get_unique_metas():
    ims = ImageStack(meta=dict(uri='testing'))

    for i in range(10):
        rimg, = get_rimg(n=1)
        meta = dict(name=f'rnd{i}', idx=i, desc='rndimg', shape=str(rimg.shape))
        ims.add_image(rimg, meta_data=meta)

    assert len(ims.unique()) == 4
    assert ims.unique()['name'] == set('rnd' + str(i) for i in range(10))
    assert ims.unique(['idx']) == dict(idx=set(i for i in range(10)))

@mark.dependency()
def test_apply_channelwise_inplace():
    ims = ImageStack(meta=dict(uri='testing'))
    
    ims = ImageStack(reader=None)
    n = 10
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': str(i), 'name': i})

    _ = apply_channelwise(ims, lambda ch: -1 * ch, inplace=True)
    
    for ori, app in zip(rimg, ims.data):
        ori = ori.squeeze()
        app = app.squeeze()
        assert np.allclose(ori + app, np.zeros_like(app))

def test_apply_channelwise_copy():
    ims = ImageStack(meta=dict(uri='testing'))
    
    ims = ImageStack(reader=None)
    n = 10
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': str(i), 'name': i})

    ret = apply_channelwise(ims, lambda ch: -1 * ch, inplace=False)
    
    for ori1, ori2, app in zip(rimg, ims.data, ret.data):
        ori1 = ori1.squeeze()
        ori2 = ori2.squeeze()
        zeros = np.zeros_like(app)
        assert np.allclose(ori1 + app, zeros)
        assert np.allclose(ori2 + app, zeros)
        assert np.allclose(ori1, ori2)

@mark.dependency(depends=['test_apply_channelwise_inplace'])
def test_apply_channelwise_skip():
    ims = ImageStack(meta=dict(uri='testing'))
    
    ims = ImageStack(reader=None)
    n = 10
    skip_name = 4, 9
    rimg = get_rimg(n, c=1)
    for i, img in enumerate(rimg):
        ndimg = imageio.core.Array(img)
        ims.add_image(ndimg, {'i': str(i), 'name': i})
    
    to_skip = [{'name': ski} for ski in skip_name]

    ret = apply_channelwise(
        image_stack=ims,
        func=lambda ch: -1 * ch,
        skip=to_skip,
        inplace=False)

    for at_ski, (ori, app) in enumerate(zip(rimg, ret.data)):
        ori = ori.squeeze()
        app = app.squeeze()
        if at_ski in skip_name:
            assert np.allclose(ori, app)
        else:
            assert np.allclose(ori, -1 * app)

def is_deepcopy(ori, cpy):
    cpy.data[0][0, 0] -= 1
    cpy.meta['HURRDURR'] = 42
    ori.meta['HURRDURR'] = 24
    data = ori.data[0][0, 0] != cpy.data[0][0, 0]
    meta = cpy.meta['HURRDURR'] != ori.meta['HURRDURR']
    return data and meta

def test_copy():
    ori = ImageStack(reader=None)

    rimg = get_rimg(n=3)
    for i, img in enumerate(rimg):
        ori.add_image(img, {'i': i, 'name': f'i{i}'})

    cpy = ori.copy()

    assert is_deepcopy(ori, cpy)

def test_astype():
    ori = ImageStack(reader=None)

    rimg = get_rimg(n=3)
    for i, img in enumerate(rimg):
        ori.add_image(img, {'i': i, 'name': f'i{i}'})
    
    newtype = ori.astype(np.float16)

    for k, w in ori.meta.items():
        assert newtype.meta[k] == w

    for ch, nch in zip(ori.data, newtype.data):
        # if fails, likely just a bad upper bound :9
        assert np.allclose(ch, nch, rtol=0.01)
        assert ch.dtype != nch.dtype

    assert is_deepcopy(ori, newtype)

def test_update_raise():
    ori = ImageStack(reader=None)

    rimg = get_rimg(n=3)
    for i, img in enumerate(rimg):
        ori.add_image(img, {'i': i, 'name': f'i{i}', 'ambi': 'ABC'})

    with raises(ValueError) as err:
        ori.update(np.zeros_like(rimg[0]), ambi='ABC')
    assert err.match('Ambiguous')

    with raises(ValueError) as err:
        ori.update(np.zeros_like(rimg[0]), NOTHING='NONE')
    assert err.match('No.*match')

    with raises(ValueError) as err:
        ori.update(np.zeros((5, 10, 1)), i=0)
    assert err.match('Invalid.*shape')


def test_update():
    ori = ImageStack(reader=None)

    rimg = get_rimg(n=3)
    for i, img in enumerate(rimg):
        ori.add_image(img, {'i': i, 'name': f'i{i}', 'ambi': 'ABC'})
    ch = ori.find_one(i=0)
    upd = np.zeros_like(rimg[0])
    assert np.any(ch != 0)
    
    ori.update(upd, name='i0', i=0)
    
    # check if really zero and if inplace
    assert np.allclose(ch, upd.squeeze())
    assert not ch is upd 
