import shutil
import copy
from pathlib import Path
import numpy as np

from pytest import mark, raises

from miscmics.multiplex import ImageStack
from miscmics.multiplex.imagedir import ProcessedReader
from miscmics.multiplex.hdf5 import HDF5Stacks


np.random.seed(1234)

class _NotDefined:

    @staticmethod
    def __eq__(*args, **kwargs):
        return False

def meta_eq(ori, other):

    ori_keys = set(ori.keys())
    other_keys = set(other.keys())
    
    assert not ori_keys.difference(other_keys)

    for key, val in ori.items():
        assert np.all(other.get(key, _NotDefined) == val)
    return True

def ims_eq(ori, other):
    assert meta_eq(ori.meta, other.meta)
    for ori_ch, other_ch in zip(ori.data, other.data):
        # checke channel meta
        assert meta_eq(ori_ch.meta, other_ch.meta)
        # checke shape
        assert ori_ch.shape == other_ch.shape
        # checke channel data
        assert np.allclose(ori_ch, other_ch)
    return True

def test_hdf5_uri_missing(small_stack, tmp_path):
    img_dir, _ = small_stack
    reader = ProcessedReader('(?P<name>.*).(?P<ext>tif)f?')
    ims = ImageStack(reader)
    ims.read_dir(img_dir)
    # keep data, remove uri
    _ = ims.meta.pop('uri')

    h5_file = Path(tmp_path) / 'dump.h5'
    assert not h5_file.exists()

    with HDF5Stacks(h5_file, 'w') as dst:
        with raises(ValueError) as err:
            dst.add_imagestack(ims)
    err.match('.*uri.*')

    assert h5_file.exists()

@mark.dependency()
def test_hdf5_write(small_stack, tmp_path):
    img_dir, _ = small_stack
    reader = ProcessedReader('(?P<name>.*).(?P<ext>tif)f?')
    ims = ImageStack(reader)
    ims.read_dir(img_dir)

    h5_file = Path(tmp_path) / 'dump.h5'
    assert not h5_file.exists()

    with HDF5Stacks(h5_file, 'w') as dst:
        dst.add_imagestack(ims)

    assert h5_file.exists()

@mark.dependency('test_hdf5_write')
def test_hdf5_cycle_single(small_stack, tmp_path):
    img_dir, _ = small_stack
    reader = ProcessedReader('(?P<name>.*).(?P<ext>tif)f?')
    ims = ImageStack(reader)

    ims.read_dir(img_dir)
    for same in copy.deepcopy(ims.data):
        same.meta['name'] += '_clone'
        ims.add_image(same)

    # test if test is valid
    assert len(ims.data) == 4

    rnd_index = np.arange(len(ims.data))
    np.random.shuffle(rnd_index)
    for i, chan in zip(rnd_index, ims.data):
        chan.meta['rnd_index'] = i

    h5_file = Path(tmp_path) / 'dump_single.h5'
    assert not h5_file.exists()

    with HDF5Stacks(h5_file, 'w') as dst:
        dst.add_imagestack(ims)

    with HDF5Stacks(h5_file, 'r') as dst:
        ims_back, = dst.read()

    assert not ims is ims_back
    assert ims_eq(ims, ims_back)

    assert np.all(rnd_index == [ch.meta['rnd_index'] for ch in ims.data])

@mark.dependency('test_hdf5_cycle_single')
def test_hdf5_cycle_multi(small_stack, tmp_path):
    img_dir, _ = small_stack
    reader = ProcessedReader('(?P<name>.*).(?P<ext>tif)f?')
    ims = ImageStack(reader), ImageStack(reader, meta=dict(uri='doubletime'))

    ims[0].read_dir(img_dir)
    ims[1].read_dir(img_dir)
    for same in copy.deepcopy(ims[1].data):
        same.meta['name'] += '_clone'
        ims[1].add_image(same)

    # test if test is valid
    assert len(ims[0].data) == 2
    assert len(ims[1].data) == 4

    rnd_index = np.arange(len(ims[1].data))
    np.random.shuffle(rnd_index)
    for i, ch in zip(rnd_index, ims[1].data):
        ch.meta['rnd_index'] = i

    h5_file = Path(tmp_path) / 'test.h5'

    with HDF5Stacks(h5_file, 'w') as dst:
        dst.add_imagestack(ims[0])
        dst.add_imagestack(ims[1])

    with HDF5Stacks(h5_file, 'r') as dst:
        ims_back = dst.read()

    for ori, red in zip(ims, ims_back):
        assert not ori is red
        assert ims_eq(ori, red)

    assert np.all(rnd_index == [ch.meta['rnd_index'] for ch in ims[1].data])
