""" Test the various entity extraction and generation methods
"""
# pylint: disable=too-many-locals,too-many-arguments,redefined-outer-name
from pytest import mark, raises, fixture
import numpy as np

from miscmics.processing.entities.extract import (
    crop_to_shape, segment_to_slice, mask_to_contour, create_from_pixmap)

from miscmics.entities import EntityLedger, ImageEntity

ledger = EntityLedger.instance()
assert ledger.entities == {}

np.random.seed(43)

testdata = [(i, j) for i in range(-4, 4) for j in range(-4, 4)]

def get_mock_masks(
        ydim=np.random.randint(40, 50),
        xdim=np.random.randint(40, 50),
        channel=np.random.randint(5, 10),
        y_0=np.random.randint(10, 35),
        dy=np.random.randint(10, 14),
        x_0=np.random.randint(10, 35),
        dx=np.random.randint(10, 14),):
    """ Create a reandom mock mask used for fixtures
    """

    y_1 = y_0 + dy
    x_1 = x_0 + dx

    img = (np.random.random(ydim * xdim * channel)
           .reshape(ydim, xdim, channel))

    slc = np.s_[y_0:y_1, x_0:x_1]
    mask = np.sum(img[slc] > 0.7, axis=-1).astype(bool)

    # check if mock is correct
    if not img[slc].shape[:-1] == mask.shape:
        raise ValueError('Mockmask is invalid')

    return slc, mask, img

@fixture
def mock_mask1():
    """ Fixture based on random mock mask
    """
    return get_mock_masks(ydim=45, xdim=52, channel=7,
                          x_0=12, dx=9, y_0=28, dy=7)

@fixture
def mock_mask2():
    """ Fixture based on random mock mask
    """
    return get_mock_masks(ydim=51, xdim=43, channel=4,
                          x_0=15, dx=10, y_0=33, dy=8)

@fixture
def mock_pixmap():
    """ Fixture based on random mock mask
    """
    return np.array([
        [0, 0, 0, 0, 0, 0, -1, -1],
        [0, 1, 1, 1, 0, 0, -1, -1],
        [0, 1, 1, 1, 0, 0,  0,  0],
        [0, 1, 1, 1, 2, 2,  2,  2],
        [0, 0, 0, 0, 2, 2,  2,  2],
        [0, 0, 0, 0, 0, 0,  2,  2],
        [0, 0, 0, 0, 0, 0,  2,  2],
        ])

def test_crop_segment_2_slice():
    """ Check if sgement_to_slice returns correct slice
    """
    core = np.array([
        [0, 0, 1, 1, 0, 0, 1],
        [0, 0, 1, 1, 0, 1, 1],
        [1, 1, 1, 1, 1, 1, 1],
        [0, 1, 1, 1, 1, 1, 0],
        [0, 1, 0, 1, 0, 0, 0],
    ])

    img = np.pad(core, [(5, 4), (2, 1)])
    slc = np.s_[5:10, 2:9]
    cnt = mask_to_contour(slc, img[slc])

    ent = ImageEntity()
    ent.update_contour(cnt)

    center_slice = segment_to_slice(ent, core.shape)

    assert np.allclose(img[center_slice], core)

@mark.parametrize('dy, dx', testdata)
def test_crop_to_shape_rnd(dy, dx, mock_mask1, mock_mask2):
    """ Test if crop_to_shape returns correct slice and mask
    """

    if dy == dx == 0:
        return

    slc, mask, img = mock_mask1
    trgt_shape = mask.shape
    slc, mask, img = mock_mask2
    _, _, channel = img.shape
    cur_y, cur_x, _ = img[slc].shape
    trgt_shape = (cur_y+dy, cur_x+dx)

    c_0, c_1 = img[slc].shape[0] // 2, img[slc].shape[1] // 2
    center_values = img[slc][c_0:c_0+2, c_1:c_1+2]
    center_mask = mask[c_0:c_0+2, c_1:c_1+2]
    values = img[slc][mask].copy()

    new_slc, new_mask = crop_to_shape(slc, mask, trgt_shape)
    new_c0 = c_0 + ((abs(dy) // 2 + abs(dy) % 2) * np.sign(dy))
    new_c1 = c_1 + ((abs(dx) // 2 + abs(dx) % 2) * np.sign(dx))
    new_center_values = img[new_slc][new_c0:new_c0+2, new_c1:new_c1+2]
    new_center_mask = new_mask[new_c0:new_c0+2, new_c1:new_c1+2]

    new_values = img[new_slc][new_mask].copy()

    # test if really a new slice
    assert new_slc != slc
    # test if really a new mask
    assert new_mask.shape != mask.shape
    # test if shape correct I
    assert img[new_slc].shape == trgt_shape + (channel,)
    # test if shape correct II
    assert new_mask.shape == trgt_shape
    # test if all values are selected, if no
    # values were clipped

    if dx > 0 and dy > 0:
        assert np.allclose(new_values, values)
    # test values in center (always there)
    assert np.allclose(center_values, new_center_values)
    # test masking in center (always there)
    assert np.all(center_mask == new_center_mask)

    # sanity, should pass if previous passed
    with raises(IndexError):
        _ = img[new_slc][mask]

    with raises(IndexError):
        _ = img[slc][new_mask]

def test_crop_to_shape_same(mock_mask1): # pylint: disable=redefined-outer-name
    """ Test if crop_to_shape returns correct slice and mask
    """

    slc, mask, img = mock_mask1
    channel = img.shape[-1]
    trgt_shape = mask.shape

    ch_0, ch_1 = img[slc].shape[0] // 2, img[slc].shape[1] // 2
    center_values = img[slc][ch_0:ch_0+2, ch_1:ch_1+2]
    center_mask = mask[ch_0:ch_0+2, ch_1:ch_1+2]
    values = img[slc][mask].copy()

    new_c0 = ch_0
    new_c1 = ch_1
    new_slc, new_mask = crop_to_shape(slc, mask, trgt_shape)
    new_center_values = img[new_slc][new_c0:new_c0+2, new_c1:new_c1+2]
    new_center_mask = new_mask[new_c0:new_c0+2, new_c1:new_c1+2]

    new_values = img[new_slc][new_mask].copy()

    # test if really a new slice
    assert not new_slc is slc
    # test if really a new mask
    assert new_mask.shape == mask.shape
    # test if shape correct I
    assert img[new_slc].shape == trgt_shape + (channel,)
    # test if shape correct II
    assert new_mask.shape == trgt_shape
    # test if all values stayed the same
    assert np.allclose(new_values, values)
    # test values in center (always there)
    assert np.allclose(center_values, new_center_values)
    # test masking in center (always there)
    assert np.all(center_mask == new_center_mask)

    # sanity, should pass if previous passed
    assert np.allclose(img[new_slc][mask], img[slc][new_mask])
    assert np.allclose(values, img[slc][new_mask])

def test_extract_entities_from_pixmap(mock_pixmap):
    """ Test create_from_pixmap and correct background_value usage
    """
    ledger = create_from_pixmap(mock_pixmap)
    assert len(ledger.entities) == 3
    all_ids = set(ent.scalars['object_id'] for ent in ledger.entities.values())
    assert all_ids == {-1, 1, 2}

    modified = mock_pixmap.copy()
    modified[mock_pixmap == 1] = -1
    modified[mock_pixmap == -1] = 3

    ledger = create_from_pixmap(modified, background_value=2)
    assert len(ledger.entities) == 3
    all_ids = set(ent.scalars['object_id'] for ent in ledger.entities.values())
    assert all_ids == {0, -1, 3}

def test_extract_entities_from_pixmap_morphology(mock_pixmap):
    """ Test create_from_pixmap correct shape of contours

    not definitive functional test, but witness for correctnes
    """
    ledger = create_from_pixmap(mock_pixmap)

    # 3x3 shaped block
    all_ones, = [ent for ent in ledger.entities.values() \
                 if ent.scalars['object_id'] == 1]
    assert all_ones.mask.shape == (3, 3)
    assert np.all(all_ones.mask == True) # pylint: disable=singleton-comparison


    # 4x4 twos
    all_twos, = [ent for ent in ledger.entities.values() \
                 if ent.scalars['object_id'] == 2]

    # rectangle...
    ought_mask = np.ones(shape=(4, 4), dtype=bool)
    # ...with lower left corner missing
    ought_mask[-2:,:2] = False
    assert np.all(all_twos.mask == ought_mask)

    # one ocntour only
    assert len(all_twos.contour) == 1

    is_points = set()
    for pnt in all_twos.contour[0]:
        is_points.add(tuple(pnt))

    want_points = set(pnt for pnt in zip(*np.where(mock_pixmap.T == 2)))
    for ipt in is_points:
        assert ipt in want_points
