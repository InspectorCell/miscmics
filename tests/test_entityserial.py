"""Tests for classes in entity serilaization classes and functions
"""
import json
from pathlib import Path
import itertools as it
from uuid import uuid4, UUID
import tempfile

from pytest import mark, fixture, raises, warns
import numpy as np

from miscmics.entities import EntityFactory, EntityType, EntityLedger
from miscmics.entities.jsonfile import EntityJSONEncoder, EntityJSONDecoder, save, load


@fixture
def get_eid():
    ledger = EntityLedger.instance()
    return ledger.get_eid

def get_entity_dict():
    import random

    eid = EntityLedger.instance().get_eid().hex
    etype = EntityType.Undefined
    ref = uuid4().hex
    tags = ['a', 'b']
    scalars = [['foo', -1], ['bar', 0.33]]
    generic = [['baz', -1], ['buzz', [0.33, 4, 1.8]], ['str', 'bernd']]
    contour = [[[random.random() for i in range(2)] for pt in range(10)] \
               for cnt in range(3)]

    return dict(eid=eid, ref=ref, tags=tags, etype=etype,
                scalars=scalars, generic=generic, contour=contour)

def get_entity_dat():

    return dict(
        eid=EntityLedger.instance().get_eid(),
        etype=EntityType.Undefined,
        ref=uuid4(),
        tags=set(['a', 'b', 'v']),
        scalars=dict(minus=1, answer=42),
        generic=dict(rnd=np.random.random((5, 3, 4)), anint=5),
        contour=np.random.random((2, 4, 2)))

def get_full_fac():
    
    tags = [['1', 'b'], ['a'], []]
    scalars = [dict(a=1), dict(a=2, b=3), dict()]
    generics = [dict(a=1), dict(a=2, b=3), dict(),
                dict(l=[1, 2, 3], bvb=np.random.random(10))]
    contours = [[np.random.random((4, 2)), np.random.random((4, 2))]]
    refs = [uuid4(), None, 'a_file']
    
    fac = EntityFactory()
    fac.ledger.clear()
        
    it_prod = it.product(tags, scalars, generics, contours, refs)
    for tgs, scl, gen, cont, rf in it_prod:
        _ = fac.create_entity(ref=rf, scalars=scl, tags=set(tgs),
                              generic=gen, contour=cont)
    return fac

@mark.dependency()
def test_to_json():
    fac = EntityFactory()
    ent0 = fac.create_entity(tags=['a', 'b', 'c'])
    enc = EntityJSONEncoder()
    ret = enc.encode(ent0)
    assert ret is not None

@mark.dependency()
def test_from_dict():
    entity_dict = get_entity_dict()

    fac = EntityFactory()
    dec = EntityJSONDecoder(factory=fac)
    ent = dec.from_dict(entity_dict)

    # ensure in ledger
    assert ent.eid in fac.ledger

@mark.dependency(depends=['test_from_dict'])
def test_from_json():
    entity_dict = get_entity_dict()
    as_json = json.dumps(entity_dict)

    fac = EntityFactory()
    dec = EntityJSONDecoder(factory=fac)
    ent = dec.decode(as_json)

    # ensure in ledger
    assert ent.eid in fac.ledger

@mark.dependency(depends=['test_to_json', 'test_from_json'])
def test_json_cycle():
    entity_dat = get_entity_dat()
    fac = EntityFactory()

    ent = fac.create_entity(**entity_dat)
    ent = fac.pop_entity(ent)

    enc = EntityJSONEncoder()
    dec = EntityJSONDecoder(factory=fac)

    ent_enc = enc.encode(ent)
    ent_cyc = dec.decode(ent_enc)

    assert ent.eid == ent_cyc.eid
    assert ent.ref == ent_cyc.ref
    assert ent_cyc.eid in fac.ledger
    assert not ent_cyc is ent
    assert np.allclose(ent_cyc.contour, ent.contour)
    assert ent_cyc.generic['anint'], ent.generic['anint']
    assert np.allclose(ent_cyc.generic['rnd'], ent.generic['rnd'])
    assert not ent_cyc.generic['rnd'] is ent.generic['rnd']


@mark.dependency(depends=['test_from_json'])
def test_from_json_missing():
    entity_dict = get_entity_dict()
    pairs = [(k, v) for k, v in entity_dict.items()]

    fac = EntityFactory()
    dec = EntityJSONDecoder(factory=fac)

    for i in range(len(pairs)):
        as_json = json.dumps(dict(pairs[:i]))
        with raises(TypeError):
            _ = dec.decode(as_json)


@mark.dependency(depends=['test_to_json'])
def test_json_file_cycle():
    
    full_fac = get_full_fac()
    before = full_fac.ledger.entities.copy()
    eids_in = set(full_fac.ledger.entities.keys())

    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpfile = Path(tmpdirname) / 'dump.ent'
        # dump everything
        entity_list = list(before.values())
        with tmpfile.open('w') as tmpf:
            json.dump(entity_list, tmpf, cls=EntityJSONEncoder)

        # clear factory, keep refs and by that ents in before
        full_fac.ledger.clear()
        
        # read everything and parse it
        with tmpfile.open('r') as tmpf:
            from_file = json.load(tmpf)
        dec = EntityJSONDecoder(factory=full_fac)
        for ent_dat in from_file:
            dec.from_dict(ent_dat)

    eids_out = set(full_fac.ledger.entities.keys())
    assert eids_in.symmetric_difference(eids_out) == set()
    for some_eid in eids_in:
        ent_out = full_fac.ledger.entities[some_eid]
        ent_in = before[some_eid]

        assert ent_out is not ent_in
        assert np.allclose(ent_out.contour, ent_in.contour)
        assert ent_out.ref == ent_in.ref
        assert ent_out.etype == ent_in.etype
        
        in_dicts = [ent_in.generic, ent_in.scalars]
        out_dicts = [ent_out.generic, ent_out.scalars]

        for in_d, out_d in zip(in_dicts, out_dicts):
            for key_in, value_in in in_d.items():
                value_out = out_d[key_in]
                if isinstance(value_out, np.ndarray):
                    assert np.allclose(value_out, value_in)
                else:
                    assert value_out == value_in

@mark.dependency(depends=['test_to_json'])
def test_save_cycle():
    
    full_fac = get_full_fac()
    before = full_fac.ledger.entities.copy()
    eids_in = set(full_fac.ledger.entities.keys())

    a_ent = next(iter(full_fac.ledger.entities.values()))
    a_ent.tags.add('test')

    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpfile = Path(tmpdirname) / 'dump.ent'
        # dump everything
        entity_list = list(before.values())

        save(tmpfile, full_fac.ledger, 'w')

        # clear factory, keep refs and by that ents in before
        full_fac.ledger.clear()
        
        # read everything and parse it
        with tmpfile.open('r') as tmpf:
            from_file = json.load(tmpf)
        dec = EntityJSONDecoder(factory=full_fac)
        for ent_dat in from_file:
            dec.from_dict(ent_dat)

    eids_out = set(full_fac.ledger.entities.keys())
    assert eids_in.symmetric_difference(eids_out) == set()
    for some_eid in eids_in:
        ent_out = full_fac.ledger.entities[some_eid]
        ent_in = before[some_eid]

        assert ent_out is not ent_in
        assert np.allclose(ent_out.contour, ent_in.contour)
        assert ent_out.ref == ent_in.ref
        assert ent_out.etype == ent_in.etype
        
        in_dicts = [ent_in.generic, ent_in.scalars]
        out_dicts = [ent_out.generic, ent_out.scalars]

        for in_d, out_d in zip(in_dicts, out_dicts):
            for key_in, value_in in in_d.items():
                value_out = out_d[key_in]
                if isinstance(value_out, np.ndarray):
                    assert np.allclose(value_out, value_in)
                else:
                    assert value_out == value_in

@mark.dependency(depends=['test_to_json'])
def test_load_save_cycle():
    
    full_fac = get_full_fac()
    before = full_fac.ledger.entities.copy()
    eids_in = set(full_fac.ledger.entities.keys())

    a_ent = next(iter(full_fac.ledger.entities.values()))
    a_ent.tags.add('test')

    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpfile = Path(tmpdirname) / 'dump.ent'
        # dump everything
        entity_list = list(before.values())

        save(tmpfile, full_fac.ledger, 'w')

        # clear factory, keep refs and by that ents in before
        full_fac.ledger.clear()
        
        # read everything and parse it
        new_factory = load(tmpfile)
        assert not new_factory is full_fac
        assert not new_factory.ledger is full_fac.ledger
    
    eids_out = set(new_factory.ledger.entities.keys())
    assert eids_in.symmetric_difference(eids_out) == set()
    for some_eid in eids_in:
        ent_out = new_factory.ledger.entities[some_eid]
        ent_in = before[some_eid]

        assert ent_out is not ent_in
        assert np.allclose(ent_out.contour, ent_in.contour)
        assert ent_out.ref == ent_in.ref
        assert ent_out.etype == ent_in.etype
        
        in_dicts = [ent_in.generic, ent_in.scalars]
        out_dicts = [ent_out.generic, ent_out.scalars]

        for in_d, out_d in zip(in_dicts, out_dicts):
            for key_in, value_in in in_d.items():
                value_out = out_d[key_in]
                if isinstance(value_out, np.ndarray):
                    assert np.allclose(value_out, value_in)
                else:
                    assert value_out == value_in


@mark.dependency(depends=['test_load_save_cycle'])
def test_load_historic():
    
    full_fac = get_full_fac()
    before = full_fac.ledger.entities.copy()
    before_len = len(before)
    
    a_ent = next(iter(full_fac.ledger.entities.values()))
    # a_eid = a_ent.eid
    a_eid = UUID(a_ent.eid.hex)
    a_ent.etype = EntityType.Historic

    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpfile = Path(tmpdirname) / 'dump.ent'
        # dump everything
        entity_list = list(before.values())

        save(tmpfile, full_fac.ledger, 'w')

        # clear factory, keep refs and by that ents in before
        full_fac.ledger.clear()
        
        # read everything and parse it
        # with pytest.warns(UserWarning) as wrn:
        new_factory = load(tmpfile, strip=False)
        # assert str(a_ent.eid) in str(wrn.pop().message)

    # check test conditions
    assert not new_factory is full_fac
    assert not new_factory.ledger is full_fac.ledger

    all_loaded_eids = [ent.eid for ent in new_factory.ledger.entities.values()]
    assert a_eid in all_loaded_eids
    assert len(all_loaded_eids) == before_len

@mark.dependency(depends=['test_load_save_cycle'])
def test_load_historic_strip():
    
    full_fac = get_full_fac()
    before = full_fac.ledger.entities.copy()
    before_len = len(before)
    
    a_ent = next(iter(full_fac.ledger.entities.values()))
    # a_eid = a_ent.eid
    a_eid = UUID(a_ent.eid.hex)
    a_ent.etype = EntityType.Historic

    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpfile = Path(tmpdirname) / 'dump.ent'

        save(tmpfile, full_fac.ledger, 'w')

        # clear factory, keep refs and by that ents in before
        full_fac.ledger.clear()
        
        # read everything and parse it
        with warns(UserWarning) as wrn:
            new_factory = load(tmpfile, strip=True)
        assert str(a_eid) in str(wrn.pop().message)

    # check test conditions
    assert not new_factory is full_fac
    assert not new_factory.ledger is full_fac.ledger

    all_loaded_eids = [ent.eid for ent in new_factory.ledger.entities.values()]
    assert a_eid not in all_loaded_eids
    assert len(all_loaded_eids) == before_len - 1
